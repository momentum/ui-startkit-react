const path = require('path');
const config = require('./base.json');
const bundleConfig = require('./bundle.json');

const _ = module.exports = {};

Object.keys(config).forEach((key) => {
	if (['port', 'host'].indexOf(key) > -1) {
		_[key] = process.env[key.toUpperCase()] || config[key];
	}
	else {
		_[key] = config[key];
	}
});

_.hasProcessFlag = (flag) => process.argv.join('').indexOf(flag) > -1;

_.cwd = (file) => path.join(process.cwd(), file || '');

_.getEnv = () => process.env.NODE_ENV;

_.env          = _.getEnv() || 'development';
_.NODE_ENV     = _.env;
_.__DEV__      = _.env === 'development';
_.__PROD__     = _.env === 'production';
_.__TEST__     = _.env === 'test';
_.__BASENAME__ = JSON.stringify(process.env.BASENAME || '');

_.isProduction = () => _.__PROD__;

_.isDevelopment = () => _.__DEV__;

_.isHmrEnabled = () => _.hasProcessFlag('hot');

_.root = path.resolve(__dirname, '..');

_.template = path.resolve(_.root, config.folders.src, config.indexFile);

_.folders = config.folders;

_.outputPath = path.join(_.root, config.folders.dist);

_.sourcePath = path.join(_.root, config.folders.src);

_.outputIndexPath = path.join(_.outputPath, config.entryFile);

_.bundleOutputPath = path.join(_.outputPath, config.indexFile);

// Bundle Helpers

_.bundleName = bundleConfig.bundleName;

_.getCssBundleFilename = () => `${_.bundleName}${_.isProduction() ? '.min' : ''}.css`;

_.getJsBundleFilename = () => `${_.bundleName}${_.isProduction() ? '.min' : ''}.js`;

_.loadersOptions = () => ({
	minimize: _.isProduction(),
	debug: _.isDevelopment(),
	options: {
		// css-loader relies on context
		context: __dirname,
	},
});
