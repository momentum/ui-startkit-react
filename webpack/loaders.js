const config = require('./settings');

exports.javascript = {
	test: /\.(js|jsx)$/,
	loaders: ['babel-loader'],
	exclude: [config.folders.npm],
};

exports.typescript = {
	test: /\.ts(x?)$/,
	loaders: ['react-hot-loader/webpack', 'awesome-typescript-loader'],
	exclude: [config.folders.npm],
};

exports.tslint = {
	test: /\.(ts|tsx)$/,
	loader: 'tslint-loader',
	enforce: 'pre',
};

exports.json = {
	test: /\.json$/,
	loader: 'json-loader',
};

exports.html = {
	test: /\.html$/,
	loader: 'raw',
	exclude: [config.folders.npm],
};

exports.fonts = {
	test: /\.(eot|otf|webp|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
	// include: [config.folders.src],
	// exclude: [config.folders.npm],
	loader: 'file-loader?limit=100000&name=fonts/[name].[ext]',
};

exports.images = {
	test: /\.(ico|jpg|jpeg|gif|png|svg)$/,
	exclude: [config.folders.npm],
	loader:'file-loader?limit=1024&name=images/[name].[ext]',
};

// exports.fonts2 = {
// 	test: /\.(otf|woff|woff2|eot|ttf)$/,
// 	exclude: config.folders.npm,
// 	loader: 'file-loader?limit=1024&name=fonts/[name].[ext]',
// };

exports.styles = [
	{
		test: /\.css$/,
		loaders: ['style-loader', 'css-loader', 'resolve-url-loader', 'postcss-loader'],
	}, {
		test: /\.scss$/,
		loaders: ['style-loader', 'css-loader', 'resolve-url-loader', 'sass-loader', 'postcss-loader'],
	},
];
