const chalk = require('chalk');
const Console = console;

// this plugin if for loggin url after each time the compilation is done.
module.exports = class LogPlugin {
	constructor(host, port) {
		this.host = host;
		this.port = port;
	}

	apply(compiler) {
		compiler.plugin('done', () => {
			Console.log(`App is running at ${chalk.yellow(`http://${this.host}:${this.port}`)}\n`);
		});
	}
};

