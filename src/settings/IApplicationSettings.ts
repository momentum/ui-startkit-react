﻿import LocalStorageTypes from 'core/localStorage/LocalStorageTypes';
import AuthenticationTypes from 'core/auth/AuthenticationTypes';
import IApiSettings from 'core/api/IApiSettings';
import IRealTimeSettings from 'core/realTime/IRealTimeSettings';

export default interface IApplicationSettings {
	title?:  string;
	icon?: string;
	description:  string;
	name: String;
	defaultRoute: string;
	version: string;
	year: number;
	lockScreenTimeout: number;
	playSounds : boolean;
	api: IApiSettings;
	realTime?: IRealTimeSettings;
	localStorageMode: LocalStorageTypes;
	authenticationMode: AuthenticationTypes;
	authorizationScope: string;
	debugMode: boolean;
	httpInterceptor: boolean;
	loginOnSignup: boolean;
	loginRedirect: string;
	signupRedirect: string;
	logoutRedirect: string;
	loginRoute: string;
	signupRoute: string;
	authHeader: string;
	authToken: string;
	tokenRoot: string;
	tokenName: string;
	tokenPrefix: string;
	analyticsId: string;
	withCredentials: boolean;
	platform: string;
	oAuthLogin?: any;
}
