﻿import LocalStorageTypes from 'core/localStorage/LocalStorageTypes';
import AuthenticationTypes from 'core/auth/AuthenticationTypes';
import IApplicationSettings from './IApplicationSettings';
// import OAuthLogin from './OAuthLogin';

const settings: IApplicationSettings = {
	title: 'Master UI',
	icon: 'graphic_eq',
	description: 'Master UI',
	name: 'master-ui',
	defaultRoute: '/',
	version: '0.0.1',
	tokenPrefix: 'masterui',
	analyticsId: process.env.GOOGLE_ANALYTICS_ID,
	api : {
		url: process.env.API_URL,
		prefix: process.env.API_PREFIX,
		contentType: 'application/json',
		grantType: 'password',
		clientId: process.env.CLIENT_ID,
		loginUrl: '/token',
		resetPasswordUrl: 'account/reset-password',
		unlinkUrl: 'auth/unlink/',
		unlinkMethod: 'get',
		signupUrl: 'account/signup',
		profileUrl: 'account/profile',
	},
	realTime : {
		host: process.env.SIGNALR_HOST,
		logging : true,
	},
	year: ((new Date()).getFullYear()),
	playSounds: false,
	lockScreenTimeout: 1,
	debugMode: process.env.DEGUB_MODE === '1',
	localStorageMode: LocalStorageTypes.Local,
	authenticationMode: AuthenticationTypes.Local,
	authorizationScope: 'openid',
	httpInterceptor: true,
	loginOnSignup: true,
	loginRedirect: '/login',
	logoutRedirect: '/logout',
	signupRedirect: '/signup',
	loginRoute: 'login',
	signupRoute: 'signup',
	tokenRoot: '',
	tokenName: 'token',
	authHeader: 'Authorization',
	authToken: 'Bearer',
	withCredentials: true,
	platform: 'browser',
	// oAuthLogin: OAuthLogin, // uncomment this line if you itend to use oAuth logins
};

export default settings;
