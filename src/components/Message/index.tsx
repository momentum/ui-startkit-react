import * as React from 'react';

interface Props {
	/** An element type to render as (string or function). */
	as?: any;

	/** Formatted to appear bordered */
	bordered?: boolean;

	/** Formatted to appear bordered */
	error?: boolean;

	/** Additional classes. */
	className?: string;

	/** Color of the icon. */
	header?: string;

	/** Messages can display a smaller corner icon. */
	children?: JSX.Element | JSX.Element[];
}

function Message (props: Props): JSX.Element {
	const {
		as = 'div',
		bordered = false,
		error = false,
		className = '',
		children,
	} = props;
	const classNames = [
		className,
		error && 'error',
		bordered && 'bordered',
	].filter(Boolean)
	.join(' ');

	const Tag = as;
	return (
		<Tag aria-hidden="true" className={classNames}>
			{ children }
		</Tag>
	);
}

export default Message;
