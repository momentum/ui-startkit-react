import * as React from 'react';
import { Image } from 'components';
import { UserProfile } from 'core/auth';

interface Props {
	user?: UserProfile;
}

export default function UserCard ({ user }: Props): JSX.Element {

	if (!Object.keys(user).length) {
		return null;
	}

	return (
		<div className="user-card">
			<Image src={`https://avatars.io/twitter/${user.username}`} className="user-card-pic" shape="circular" />
			<div className="user-card-details">
				<span className="user-greetings">Welcome, {user.firstName}</span>
			</div>
		</div>
	);
}
