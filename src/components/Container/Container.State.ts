export default interface ContainerState<T> {
	editingModel?: T;
	errors?: any;
}
