type RouteParams = {
	id?: string;
	action?: string;
};

export default RouteParams;

