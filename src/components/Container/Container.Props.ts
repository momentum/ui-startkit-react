import { RouteComponentProps } from 'react-router';
import RouteParams from './Container.RouteParams';

export default interface ContainerProps<T, P extends RouteParams> extends RouteComponentProps<P> {
	isLoading?: boolean;
	isSaving?: boolean;
	models: T[];
	selectedModel: T;
	checkedModels?: Array<string | number>;
}
