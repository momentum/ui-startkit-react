import * as React from 'react';
import { autobind } from 'core-decorators';

import ContainerState from './Container.State';
import ContainerProps from './Container.Props';

@autobind
export default abstract class MasterContainer<T, P extends ContainerProps<T, any>, S extends ContainerState<T>> extends React.Component<P, S> {

	protected domain?: string;
	protected key?: string;
	protected basePath?: string;
	protected allowedActions?: string[] = ['new', 'edit'];
	protected toggleSelection?: Function;
	protected readonly newModel: T = {} as T;

	constructor (props: P) {
		super(props);

		this.basePath = props.match.path.match(/[^:]*/i)[0];
	}

	componentWillReceiveProps(nextProps: P) {
		const { id, action } = nextProps.match.params;

		// if an action is set but it's not allowed, redirect to the domain base url
		if (action && !this.isAllowedAction(action)) {
			return this.props.history.replace(this.basePath);
		}

		// tslint:disable-next-line: triple-equals
		const selectedModel = nextProps.models
			? nextProps.models.find((item) => item[this.key] == id)
			: nextProps.selectedModel;

		if (typeof this.toggleSelection === 'function') {
			this.toggleSelection(selectedModel);
		}

		// if action is eihter edit or new, set the editing model to the state
		// otherwise, model is display only
		const editingModel = action === 'edit'
			? selectedModel
			: action === 'new'
				? this.newModel
				: undefined;

		this.setState({ editingModel });
	}

	protected isAllowedAction = (action) => this.allowedActions.indexOf(action) > -1;

	isNew (model): boolean {
		return !this.props.models.find((item) => item[this.key] === model[this.key]);
	}

	/**
	 * Handles logic on operations which envolves url changes
	 * @param {T} [selectedModel]
	 * @param {string} [action]
	 */
	handleAction(selectedModel?: T, action?: string) {
		const key = selectedModel[this.key] || '0';
		const url = this.isAllowedAction(action)
			? `${key}/${action}`
			: !action ? key : '';
		const finalUrl = `${this.basePath}//${url}`
			.replace(/\/{2,}/g, '/') // replace double slashes by single
			.replace(/\/$/, ''); // remove last forward slash

		this.setState({ editingModel: selectedModel }, () => {
			this.props.history.replace(finalUrl);
		});
	}
}
