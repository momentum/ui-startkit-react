import * as React from 'react';
import { autobind } from 'core-decorators';
import { Icon } from 'components';
import IMenuItem from 'core/interfaces/IMenuItem';
import './index.scss';

interface Props {
	position: string;
	items: Array<IMenuItem>;
	activeItem?: string;
	submenuTrigger?: string;
	onRouteChange?: (to: string) => void;
}

interface State {
	pinedItems: Array<any>;
}

@autobind
export default class Menu extends React.Component<Props, State> {

	static props = {
		position: 'vertical',
		submenuTrigger: 'click',
	};

	constructor(props: Props) {
		super(props);

		this.state = {
			pinedItems: [],
		};
	}

	triggerMenu (event, href) {
		event.stopPropagation();
		const { target } = event;
		const { pinedItems } = this.state;
		const { onRouteChange } = this.props;
		const el = target.closest('.menu-item');
		const elParent = el.closest('.menu-parent');
		const elParentId = elParent ? elParent.getAttribute('id') : undefined;

		if (pinedItems.length) {
			pinedItems.forEach(({id, el}) => elParentId ? id !== elParentId : true && el.classList.remove('expanded'));
		}

		if (elParent === el) {
			elParent.classList.toggle('expanded');
			pinedItems.push({id: elParentId, el: elParent});
		} else {
			if (typeof href === 'function') {
				href();
			} else {
				onRouteChange(href);
			}
		}
	}

	getMenuItem ({ id, href, label, icon, children = [], isChild = false }) {
		const { activeItem } = this.props;
		const hasChildren = children.length > 0;
		const classNames = [
			'menu-item',
			hasChildren && 'menu-parent',
			typeof href === 'string' && href === activeItem && 'active',
		].filter(Boolean).join(' ');

		return (
			<li key={id}
				id={`menu-${id}`}
				className={classNames}
				title={label}
				onClick={(event) => this.triggerMenu(event, href)}
			>
				<span className="menu-link">
					{hasChildren && <span className="menu-expander"><Icon name="play_arrow"></Icon></span>}
					<span className="menu-icon">{ icon && <Icon name={icon} /> }</span>
					<span className="menu-label">{ label }</span>
				</span>
				{ hasChildren &&
				<ul className="submenu">
					{ children.map((item) => this.getMenuItem({...item, isChild: true})) }
				</ul>
				}
			</li>
		);
	}

	render(): JSX.Element {
		const { position, items, submenuTrigger } = this.props;
		return (
			<ul className={`menu ${position} expand-on-${submenuTrigger}`}>
				{items.map(this.getMenuItem)}
			</ul>
		);
	}
}
