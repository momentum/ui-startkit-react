import * as React from 'react';
import { Logo, Menu, Icon, FullScreen } from 'components';
import IMenuItem from 'core/interfaces/IMenuItem';
// import { UserProfile } from 'core/auth';
import './index.scss';

interface Props {
	collapsed: boolean;
	routes?: Array<IMenuItem>;
	title?: string;
	icon?: string;
	showLogo: boolean;
	children?: string | JSX.Element;
	extraInfo?: JSX.Element;
	activeRoute?: string;
	onTogglePanel?: () => void;
	onOpenSettings?: () => void;
	onRouteChange?: (to: string) => void;
}

export default function Sidebar ({
	title,
	icon,
	routes,
	collapsed = false,
	children,
	showLogo,
	extraInfo,
	activeRoute,
	onTogglePanel,
	onOpenSettings,
	onRouteChange,
}: Props): JSX.Element {
	const classNames = [
		'sidebar',
		collapsed && 'collapsed',
	].filter(Boolean).join(' ');

	return (
		<aside className={classNames}>
			{showLogo && <div className="sidebar-header">
				<Logo title={title} icon={icon} onClick={() => onRouteChange('') }/>
			</div>}
			{extraInfo &&
			<div className="sidebar-extra">
				{extraInfo}
			</div>
			}
			<nav className="sidebar-options">
				{ children }
				{ routes &&
					<Menu
						items={routes}
						position="vertical"
						activeItem={activeRoute}
						submenuTrigger="hover"
						onRouteChange={onRouteChange}
					/>
				}
			</nav>
			<footer className="sidebar-footer">
				<span title="Expand/Collapse" onClick={onTogglePanel} className="sidebar-footer_item toggle-collapse">
					<Icon name="chevron_right" className="sidebar-logo" aria-hidden="true" />
				</span>
				<span title="Settings" onClick={onOpenSettings} className="sidebar-footer_item open-settings">
					<Icon name="settings" aria-hidden="true" />
				</span>
				<FullScreen title="FullScreen" iconOff="fullscreen_exit" iconOn="fullscreen" className="sidebar-footer_item toggle-fullscreen" />
			</footer>
		</aside>
	);
}
