import * as React from 'react';
import shadeBlendConvert from './shadeBlendConvert';
import './index.scss';

interface Props {
	id?: string;
	label?: string;
	value?: boolean;
	onChange?: (event: any) => void;
}

export default function ColorSchemaPicker({
	id = 'choose-theme-color',
	label = '',
	value,
	onChange,
}: Props): JSX.Element {

	const themeColorProp = 'themeColor';
	const themeLightColorProp = 'themeColorLight';
	const themeLighterColorProp = 'themeColorLighter';
	const themeLightestColorProp = 'themeColorLightest';
	const factor = 0.25;

	function setStyleProperty (prop: string, value: string) {
		document.documentElement.style.setProperty(prop, value);
	}

	function setColorProperties (value: string) {
		setStyleProperty(`--${themeColorProp}`, value);
		setStyleProperty(`--${themeLightColorProp}`, value && shadeBlendConvert(value, factor));
		setStyleProperty(`--${themeLighterColorProp}`, value && shadeBlendConvert(value, factor * 2));
		setStyleProperty(`--${themeLightestColorProp}`, value && shadeBlendConvert(value, factor * 3));
		localStorage.setItem(themeColorProp, value);

		if (typeof onChange === 'function') {
			onChange(value);
		}
	}

	function handleChange (e: React.ChangeEvent<HTMLInputElement>) {
		e.preventDefault();
		e.stopPropagation();
		const { value } = e.target;
		setColorProperties(value);
	}

	function resetColor (e: any) {
		e.preventDefault();
		e.stopPropagation();
		localStorage.removeItem(themeColorProp);
		localStorage.removeItem(themeLightColorProp);
		localStorage.removeItem(themeLighterColorProp);
		localStorage.removeItem(themeLightestColorProp);
		setColorProperties(null);
	}

	function init () {
		const value = localStorage.getItem(themeColorProp);
		if (value && value !== 'null') {
			setColorProperties(value);
		}
	}

	init();

	return (
		<div className="color-schema-picker-container">
			{ label && <label>{ label }</label>}
			<span>
				<input type="color" id={id} onChange={ handleChange } />
				<a href="javascript:void(0)" onClick={resetColor} title="Reset Theme Color">×</a>
			</span>
		</div>
	);

}
