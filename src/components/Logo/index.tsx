import * as React from 'react';
import { Icon, Image } from 'components';
import './index.scss';

type SizeProp = 'mini' | 'small' | 'large' | 'big';
type DisplayProp = 'inline' | 'block';
type AsProp = 'icon' | 'image';

interface Props {
	as?: AsProp;
	size?: SizeProp;
	display?: DisplayProp;
	title?: string;
	icon?: string;
	imageUrl?: string;
	onClick?: Function;
}

export default function Logo ({
	as = 'icon',
	size = 'small',
	display = 'inline',
	title = '',
	icon = '',
	imageUrl,
	onClick,
}: Props): JSX.Element {

	function isClickable () { return typeof onClick === 'function'; }

	const classNames = [
		'logo',
	].filter(Boolean).join(' ');

	function handleClick() {
		if (isClickable()) {
			onClick();
		}
	}

	return (
		<div className={classNames} data-size={size} data-display={display} data-as={as} onClick={handleClick}>
			{ icon &&
			<span className="logo-icon">
				<Icon name={icon}/>
			</span>
			}
			{ (!icon && imageUrl) &&
			<span className="logo-icon">
				<Image src={imageUrl} onClick={onClick} alt={title} />
			</span>
			}
			{ title &&
			<span className="logo-title">
				{ title }
			</span>
			}
		</div>
	);
}
