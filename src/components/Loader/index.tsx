import * as React from 'react';

type LoaderSizeProp = 'mini' | 'tiny' | 'small' | 'large' | 'big' | 'huge' | 'massive';

interface Props {
	[key: string]: any;

	/** An element type to render as (string or function). */
	as?: any;

	/** Formatted to appear bordered */
	bordered?: boolean;

	/** Loader can formatted to appear circular. */
	circular?: boolean;

	/** Additional classes. */
	className?: string;

	/** Color of the icon. */
	color?: string;

	/** Loaders can display a smaller corner icon. */
	corner?: boolean;

	/** Show that the icon is inactive. */
	disabled?: boolean;

	/** Fitted, without space to left or right of Loader. */
	fitted?: boolean;

	/** Loader can flipped. */
	flipped?: 'horizontally' | 'vertically';

	/** Formatted to have its colors inverted for contrast. */
	inverted?: boolean;

	/** Loader can be formatted as a link. */
	link?: boolean;

	/** Loader can be used as a simple loader. */
	loading?: boolean;

	/** Name of the icon. */
	name?: string;

	/** Loader can rotated. */
	rotated?: 'clockwise' | 'counterclockwise';

	/** Size of the icon. */
	size?: LoaderSizeProp;
}

function Loader (props: Props): JSX.Element {
	const {
		// name = 'compress',
		// key,
		// as = '',
		// bordered = '',
		// circular = '',
		// className = '',
		// color = '',
		// corner = '',
		// disabled = '',
		// fitted = '',
		// flipped = 'horizontally',
		// inverted = '',
		// link = '',
		// loading = '',
		// rotated = 'clockwise',
	} = props;

	return (
		<div className="loader-circle-wrapper">
			<svg className="loader-circle" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
				<circle className="path" fill="none" strokeWidth="6" strokeLinecap="round" cx="33" cy="33" r="30"></circle>
			</svg>
		</div>
	);
}

export default Loader;
