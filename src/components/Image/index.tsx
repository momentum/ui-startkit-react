import * as React from 'react';
// import { Image } from 'components';

type ImageSizeProp = 'mini' | 'tiny' | 'small' | 'large' | 'big' | 'huge' | 'massive';

interface Props {
	[key: string]: any;

	/** Additional classes. */
	className?: string;

	/** Name of the Image. */
	id?: string;

	/** Name of the Image. */
	shape?: 'squared' | 'circular';

	/** source of the Image. */
	src?: string;

	size?: ImageSizeProp;
}

function Image (props: Props): JSX.Element {
	const {
		key,
		id,
		src = '',
		className = '',
		shape = 'squared',
		size,
	} = props;
	// return <Image {...props} />;
	const classNames = [
		shape, size,
	].filter(Boolean).join(' ');

	return (
		<span className={className} aria-hidden="true" >
			<img id={id} key={key} src={src} className={classNames} />
		</span>
	);
}

export default Image;
