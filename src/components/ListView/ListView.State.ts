export default interface ListViewState<T> {
	keyword?: string;
	filteredModels?: T[];
}
