import * as React from 'react';
import { autobind } from 'core-decorators';
import { isFunction, kebabCase } from 'lodash';

import Loader from '../Loader';
import ListViewState from './ListView.State';
import ListViewProps from './ListView.Props';

import './index.scss';

@autobind
export default abstract class MasterListView<T, P extends ListViewProps<T>, S extends ListViewState<T>> extends React.Component<P, S> {

	protected domain?: string;
	protected key?: string;
	protected className?: string | string[];
	protected filterKeys?: string[] = [];

	constructor (props: P) {
		super(props);
	}

	componentDidMount () {
		const { onGet } = this.props;

		if (isFunction(onGet)) {
			onGet();
		}
	}

	public render(): JSX.Element {
		const { isLoading, models } = this.props;
		const { keyword } = this.state;
		const classNames = ['panel-list', this.className ].filter(Boolean).join(' ');

		const filteredModels = !keyword
			? models
			: models.reduce((result, model) => {
				const matches = this.filterKeys.filter((key) => model[key].toString().toLowerCase().indexOf(keyword.toString().toLowerCase()) > -1).length > 0;
				if (matches) {
					result.push(model);
				}
				return result;
			}, []);

		return (
			<div id={`panel-list-${kebabCase(this.domain)}`} className={classNames}>
				{isLoading && <Loader active inline /> }
				{!isLoading && filteredModels.map(this.handleRenderListModel)}
			</div>
		);
	}

	protected abstract renderListModel(model: T): JSX.Element;

	handleRenderListModel (item: any, index: number) {
		const idProp = this.key;
		const { selectedModel, checkedModels, onSelect, onCheck } = this.props;
		const isModelSelected = Object.keys(selectedModel).length > 0 && selectedModel[idProp] === item[idProp];
		const classNames = [
			'panel-list-item',
			isModelSelected && 'list-item-selected',
		].filter(Boolean).join(' ');

		return (
			<div
				key={item[idProp] || index}
				className={classNames}
				onClick={() => isFunction(onSelect) && onSelect(item)}
			>
				{ isFunction(onCheck) && (
				<div className="panel-list-item-checkbox">
					<input
						id={`${kebabCase(item.name)}-checkbox`}
						type="checkbox"
						checked={checkedModels.indexOf(item[idProp]) > -1}
						onChange={({target}) => onCheck(item[idProp], target.checked)}
					/>
				</div>
				)}
				{isFunction(this.renderListModel) && this.renderListModel(item)}
			</div>
		);
	}
}
