export default interface ListViewProps<T> {
	models: T[];
	selectedModel: T;
	checkedModels?: Array<string | number>;
	isLoading?: boolean;
	isSaving?: boolean;
	onSelect?: (item: any) => void;
	onCheck?: (id: string | string[], checked: boolean) => void;
	onGet: (reload?: boolean) => Promise<T[]>;
	onCreate: () => void;
	onDelete: () => void;
}
