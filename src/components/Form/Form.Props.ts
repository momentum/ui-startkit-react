export default interface FormProps<T> {
	isSaving?: boolean;
	model: T;
	errors?: any;
	onSubmit: (item: T) => Promise<any>;
	onCancel?: () => void;
}
