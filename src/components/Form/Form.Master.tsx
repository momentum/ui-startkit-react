import * as React from 'react';
import { autobind } from 'core-decorators';
import Ajv from 'ajv';
import { kebabCase, isArray, isError } from 'lodash';
import Button from 'components/Button';
import Logger from 'core/logger/LoggerFactory';
import IField from './IField';
import FormState from './Form.State';
import FormProps from './Form.Props';
import FormControlEvent from './Form.ControlEvent';

import './index.scss';

interface Param {
	missingProperty: any;
}

interface Err extends Ajv.ErrorObject {
	params: Param;
}

type FormLayout = 'row' | 'column';

@autobind
/**
 * @class MasterForm
 * @extends React.Component
 */
export default abstract class MasterForm<T, P extends FormProps<T>, S extends FormState<T>> extends React.Component<P, S> {

	private ajv: Ajv.Ajv = Ajv({ allErrors: true, validateSchema: true });
	private validator: Ajv.ValidateFunction;
	protected domain?: string;
	protected key?: string;
	protected id?: string;
	protected className?: string | string[];
	protected schema: any;
	protected layout?: FormLayout = 'row';

	constructor (props: P, context) {
		super(props, context);
	}

	componentDidMount() {
		this.validator = this.ajv.compile(this.schema);
	}

	public render (): JSX.Element {
		const { actions = [], fields, errors } = this.state;
		const { isSaving } = this.props;
		const classNames = ['form-group', this.className, this.layout].filter(Boolean).join(' ');

		const globalError = isError(errors) ? (errors as Error).message : undefined;

		return (
			<fieldset disabled={isSaving}>
				<form id={this.id} className={classNames}>
				{fields
					.filter((f) => ((f as IField).id || React.isValidElement(f)) && f.type !== 'hidden')
					.map((field, index) => {
						if (React.isValidElement(field)) {
							return field;
						}
						return (
							<div key={`field-${index}`} className={this.getCssClasses(field)}>
								{ field.label &&
								<label htmlFor={field.id} className="form-field_label">
									{field.label}
									{this.isRequired(field.id) && <span title={field.hint || 'Required field'} className="form-field_required">•</span>}
									{field.type === 'checkbox' && this.getFieldMarkup(field)}
								</label>
								}
								{ field.type !== 'checkbox' &&
								<div className="form-field_item">
									{this.getFieldMarkup(field)}
									{errors && this.getValidationMessages(field)}
								</div>
								}
							</div>
						);
					})
				}
				{globalError ? <div className="form-error">{globalError}</div> : null}
				{!actions.length ? null :
					<div className="form-actions">
						{ actions.map(({id, ...props}, index) => <Button key={id || index} {...props} />) }
					</div>
				}
				</form>
			</fieldset>
		);
	}

	/**
	* @method onChange
	* @description Handles fields change event and set its correspondent model property value
	* @param event onChange event object
	*/
	protected onChange = (event: FormControlEvent): void => {
		const { type, name, value } = event.currentTarget;
		let v: string | number | boolean;

		switch (type) {
			case 'number':
				v = !isNaN(value as any) ? Number(value) : '';
				break;
			case 'checkbox':
				v = (event.currentTarget as HTMLInputElement).checked;
				break;
			default:
				v = value;
		}

		const model: T = Object.assign({}, this.state.model, {[name]: v});

		this.setState({ model } as S);
	}

	private errorToProperty = (err: Err) => {
		const { params = { missingProperty: null } } = err;

		return (params.missingProperty)
			? params.missingProperty
			: undefined;
	}

	/**
	* @method validate
	* @description Validate form fields based on its json schmema validator
	* @param id Field id
	*/
	protected validate = (value): boolean => {
		const valid = this.validator(value);
		if (valid) {
			return true;
		}

		const errors = this.validator.errors.reduce((result, err: Err) => {
			const prop = this.errorToProperty(err);
			const path = err.dataPath
				? err.dataPath.substr(1)
				: null;
			const fullPath = path && prop
				? `${path}.${prop}`
				: path || prop;

			result[fullPath] = {...err, path: fullPath };

			return result;
		}, {});

		this.setState({ errors });

		return false;
	}
		// tslint:disable-next-line: triple-equals
	/**
	* @method isRequired
	* @description Based on model schema, checks whether a field is required
	* @param id Field id
	*/
	private isRequired = (id: string): boolean => (
		this.schema.required.findIndex((item) => item === id) > -1
	)

	/**
	* @method getCssClasses
	* @description Format the field css classes to be applied to the field
	* @param className Css class to be added to the field markup, besides of the default form-field
	*/
	private getCssClasses = ({className}: IField): string => (
		['form-field', className].filter(Boolean).join(' ')
	)

	/**
	 * @method getValidationMessages
	 * @description Returns a list of field validation messages
	 * @param field Field object from which validation messages are gotten from
	 */
	private getValidationMessages (field: IField): JSX.Element {
		const { message } = this.state.errors[field.id] || { message: undefined };
		const messages = message ? (isArray(message) ? message : [message]) : undefined ;

		return (
			message &&
			<ul className="form-field_error">
				{messages.map((m) => <li key={kebabCase(message)}>{m}</li>)}
			</ul>
		);
	}

	/**
	 * @method getFieldMarkup
	 * @description Returns the field markup
	 * @param type: Field type
	 * @param label: Label to identify what the field is about
	 * @param options: Select field options
	 * @param ..options: Deconstruct the rest of the field properties
	 */
	private getFieldMarkup ({type, label, options = [], ...props }: IField) {
		const { model } = this.state;
		const inputProps = Object.assign({}, props,
			{
				title: label,
				onChange: this.onChange,
			},
			type === 'checkbox'
				? { checked: model[props.name] }
				: { value: model[props.name] != undefined // tslint:disable-line: triple-equals
					? model[props.name]
					: '',
				},
		);

		switch (type) {
			case 'text':
			case 'checkbox':
			case 'number':
			case 'radio':
			case 'hidden':
			case 'password':
				return <input type={type} {...inputProps} />;
			case 'textarea':
				return <textarea {...inputProps} />;
			case 'date':
			case 'datetime':
				break;
			case 'select':
				return (
					<select {...inputProps}>
						{options.map(({label, value}) => <option key={value} value={value}>{label}</option>)}
					</select>
				);
		}
	}

	/**
	 * @method handleSave
	 * @description Handles the form save action. Triggers form validation and in case of valid, submit the form
	 * @param event: Field typeSave action button event
	 * @returns {Promise<void>}
	 */
	protected async handleSave (event: Event): Promise<void> {
		const { onSubmit } = this.props; // onCancel
		const { model } = this.state;
		const isValid: boolean = this.validate(model);

		if (isValid) {
			const response = await onSubmit(model) || {};
			if (response.message) {
				Logger.info(response.message);
			}
			// await onCancel();
		}
	}
}
