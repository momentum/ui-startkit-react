type HTMLInput = HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement;

export default interface FormControlEvent extends React.FormEvent<HTMLInput> {
	currentTarget: HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement;
}
