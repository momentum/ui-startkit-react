export default interface IFieldOptions {
	label: string;
	value: string;
}
