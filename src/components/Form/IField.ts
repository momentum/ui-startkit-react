import IFieldOptions from './IFieldOptions';

export default interface IField {
	id: string;
	name: string;
	type: string;
	label: string;
	className?: string;
	disabled?: boolean;
	icon?: string;
	placeholder?: string;
	hint?: string;
	value?: string | number | string[] | boolean;
	maxLength?: number;
	order?: number;
	options?: IFieldOptions[];
	required?: boolean;
	validationMessages?: string[];
}
