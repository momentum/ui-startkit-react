import IField from './IField';
import ButtonProps from 'components/Button/ButtonProps';

export default interface FormState<T> {
	fields: IField[];
	actions?: ButtonProps[];
	model: T;
	errors?: any;
	validator?: any;
}
