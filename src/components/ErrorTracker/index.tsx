import './index.scss';

interface Props {
	debugMode: boolean;
	error: Error;
	onClearError: () => void;
}

export default function ErrorTracker ({debugMode, error, onClearError }: Props): JSX.Element {
	if (!debugMode || !error) {
		return null;
	}
	console.error(error);
	// onClearError();
	return null;
}

