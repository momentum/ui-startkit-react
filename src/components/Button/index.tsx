import * as React from 'react';
import Icon from 'components/Icon';
import ButtonProps from './ButtonProps';

import './index.scss';

export default function Button ({
	id,
	as = 'button',
	bordered,
	icon,
	label,
	className = '',
	title = '',
	disabled = false,
	iconOnly = false,
	size = 'small',
	onClick,
	confirmationMessage,
}: ButtonProps): JSX.Element {
	const classNames = [
		'button',
		className,
		iconOnly && 'icon-only',
	].filter(Boolean).join(' ');

	function handleClick(event) {
		event.preventDefault();
		if (typeof onClick !== 'function') {
			throw new Error('A click action hasn\'t been defined for this button');
		}

		if (!confirmationMessage) {
			onClick(event);
		}
		else {
			if (window.confirm(confirmationMessage)) {
				onClick(event);
			}
		}
	}

	return (
		<button
			type="button"
			key={id}
			id={id}
			disabled={disabled}
			title={ title || (iconOnly ? label : '') }
			onClick={handleClick}
			className={classNames}
		>
			{icon && <Icon name={icon} />}
			{!iconOnly ? label : ''}
		</button>
	);
}
