
export type ButtonSizeProp = 'small' | 'medium' | 'large';

export default interface ButtonProps {
	[id: string]: any;

	/** An element type to render as (string or function). */
	as?: any;

	/** Formatted to appear bordered */
	bordered?: boolean;

	/** Icon can formatted to appear circular. */
	icon?: string;

	/** Label of the button. */
	label?: string;

	title?: string;

	iconOnly?: boolean;

	/** Additional classes. */
	className?: string;

	/** Show that the icon is inactive. */
	disabled?: boolean;

	/** Display order. */
	order?: number;

	/** Name of the icon. */
	onClick?: (item?: any) => any;

	/** Size of the icon. */
	size?: ButtonSizeProp;

	/** Request a confirmation before executing the action. */
	confirmationMessage?: string;
}
