import * as React from 'react';
import './index.scss';

interface Props {
	position?: string;
	children?: any;
}

export default function SlidingPanel ({ children, position = 'right' }: Props): JSX.Element {
	const elTag = 'sliding-panel';

	const handleOnChange = ({target : {checked}}) => {
		const container = document.getElementById(elTag);
		if (container instanceof HTMLElement) {
			container.classList[checked ? 'add' : 'remove']('expanded');
		}
	};

	return (
		<div id={elTag} className={position} >
			<div className={`${elTag}-trigger`}>
				<input type="checkbox" id={`${elTag}-pin`} className={`${elTag}-trigger-checkbox`} onChange={handleOnChange}/>
				<label htmlFor={`${elTag}-pin`} className={`${elTag}-trigger-pinner`}>
					<i className="pin icon"></i>
				</label>
			</div>
			<div id={`${elTag}-content`}>
				{ children }
			</div>
		</div>
	);
}

