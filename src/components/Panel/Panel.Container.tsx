import * as React from 'react';
import { ReflexContainer } from 'react-reflex';

import './index.scss';

interface Props {
	id?: string;
	orientation?: string;
	className?: string;
	style?: object;
	children?: JSX.Element | JSX.Element[];
}

export default class Container extends React.Component<Props> {
	constructor (props: Props) {
		super(props);
	}

	static defaultProps = {
		orientation: 'vertical',
	};

	render() : JSX.Element {
		const { className, children, ...rest} = this.props;
		const classNames = ['panel', className].filter(Boolean).join(' ');

		return (
			<ReflexContainer className={classNames} {...rest} >
				{ children }
			</ReflexContainer>
		);
	}
}
