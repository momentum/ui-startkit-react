import * as React from 'react';
import { autobind } from 'core-decorators';
import { IKey } from 'core/data';
import Loader from '../Loader';

import { isFunction, kebabCase } from 'lodash';

import './index.scss';

interface Props {
	idProp?: string;
	domain?: string;
	isLoading: boolean;
	items: Array<any>;
	selectedItem: any;
	checkedItems?: Array<IKey>;
	onCheck?: (id: IKey | IKey[], checked: boolean) => void;
	onFetch?: () => {};
	onRenderListItem: (item: any) => JSX.Element;
	onSelect?: (item: any) => void;
}

interface State {
}

@autobind
export default class ListPanel extends React.Component<Props, State> {

	static defaultProps = {
		idProp: 'id',
		domain: 'Item',
		selectedItem: {},
		checkedItems: [],
	};

	static state: State = {};

	constructor(props: Props) {
		super(props);
		this.state = {};
	}

	componentDidMount () {
		const { onFetch } = this.props;

		if (isFunction(onFetch)) {
			onFetch();
		}
	}

	render(): JSX.Element {
		const { domain, items, isLoading } = this.props;

		return (
			<div id={`panel-list-${kebabCase(domain)}`} className="panel-list">
				{isLoading && <Loader active inline /> }
				{!isLoading && items.map(this.handleRenderListItem)}
			</div>
		);
	}

	handleRenderListItem (item: any, index: number) {
		const { idProp, selectedItem, checkedItems, onSelect, onCheck, onRenderListItem } = this.props;
		const isItemSelected = Object.keys(selectedItem).length > 0 && selectedItem[idProp] === item[idProp];
		const classNames = [
			'panel-list-item',
			isItemSelected && 'list-item-selected',
		].filter(Boolean).join(' ');

		return (
			<div
				key={item[idProp] || index}
				className={classNames}
				onClick={() => isFunction(onSelect) && onSelect(item)}
			>
				{ isFunction(onCheck) && (
				<div className="panel-list-item-checkbox">
					<input
						id={`${kebabCase(item.name)}-checkbox`}
						type="checkbox"
						checked={checkedItems.indexOf(item[idProp]) > -1}
						onChange={({target}) => onCheck(item[idProp], target.checked)}
					/>
				</div>
				)}
				{isFunction(onRenderListItem) && onRenderListItem(item)}
			</div>
		);
	}
}
