export type FilterOption = {
	value: string;
	label: string;
	disabled?: boolean;
};

export default interface IFilter {
	id: string;
	type: string;
	className?: string;
	placeholder?: string;
	options?: FilterOption[];
	order?: number;
	disabled?: boolean;
	onChange: (event: any) => void;
}
