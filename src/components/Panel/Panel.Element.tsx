import * as React from 'react';
import { autobind } from 'core-decorators';
import { ReflexElement } from 'react-reflex';
import { Button, Loader } from 'components';
import { Action, Filter } from './';

import './index.scss';

interface Props {
	className?: string;
	actions?: Action[];
	filters?: Filter[];
	isLoading?: boolean;
	children?: JSX.Element | JSX.Element[];
}

@autobind
export default class Element extends React.Component<Props> {
	constructor (props: Props) {
		super(props);
	}

	render() : JSX.Element {
		const { isLoading, className, children, actions = [], filters = []} = this.props;
		const classNames = ['panel-element', className].filter(Boolean).join(' ');
		const filtersBar = !filters.length ? null :
			<div className="panel-toolbar_filters">
				{ this.getFilterElement(filters) }
			</div>;
		const actionsBar = !actions.length ? null :
			<div className="panel-toolbar_actions">
				{ actions.map(({id, ...props}, index) => <Button key={id || index} {...props} />) }
			</div>;

		return (
			<ReflexElement className={classNames} >
				{ (filtersBar || actionsBar) &&
					<div className="panel-toolbar">
						{ filtersBar }
						{ actionsBar }
					</div>
				}
				<div className="panel-body">
					{isLoading && <Loader active inline /> }
					{!isLoading && children }
				</div>
			</ReflexElement>
		);
	}

	getFilterElement(filters: Filter[]): JSX.Element | JSX.Element[] {
		return filters.reduce((result, {id, type, options, onChange, ...rest}: Filter, index) => {
			switch (type) {
				case 'text':
					result.push(<input key={id || index}  type="text" onChange={onChange} {...rest} />);
					break;
				case 'select':
					result.push(
						<select id={id} onChange={onChange} {...rest}>
							{ options.map(({value, label}) => <option key={label} value={value}>{label}</option>)}
						</select>,
					);
					break;
			}
			return result;
		}, []);
	}
}
