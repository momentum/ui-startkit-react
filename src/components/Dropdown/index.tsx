import * as React from 'react';

type DropdownSizeProp = 'mini' | 'tiny' | 'small' | 'large' | 'big' | 'huge' | 'massive';

interface Props {
	[key: string]: any;

	/** An element type to render as (string or function). */
	as?: any;

	/** Formatted to appear bordered */
	bordered?: boolean;

	/** Dropdown can formatted to appear circular. */
	circular?: boolean;

	/** Additional classes. */
	className?: string;

	/** Color of the dropdown. */
	color?: string;

	/** Dropdowns can display a smaller corner dropdown. */
	corner?: boolean;

	/** Show that the dropdown is inactive. */
	disabled?: boolean;

	/** Fitted, without space to left or right of Dropdown. */
	fitted?: boolean;

	/** Dropdown can flipped. */
	flipped?: 'horizontally' | 'vertically';

	/** Formatted to have its colors inverted for contrast. */
	inverted?: boolean;

	/** Dropdown can be formatted as a link. */
	link?: boolean;

	/** Dropdown can be used as a simple loader. */
	loading?: boolean;

	/** Name of the dropdown. */
	name?: string;

	/** Dropdown can rotated. */
	rotated?: 'clockwise' | 'counterclockwise';

	/** Size of the dropdown. */
	size?: DropdownSizeProp;
}

function Dropdown ({
	name = 'compress',
	key,
	as = '',
	bordered = '',
	circular = '',
	className = '',
	color = '',
	corner = '',
	disabled = '',
	fitted = '',
	flipped = 'horizontally',
	inverted = '',
	link = '',
	loading = '',
	rotated = 'clockwise',
}: Props): JSX.Element {
	return <span aria-hidden="true" ><i className={`fa fa-${name}`}></i></span>;
}

export default Dropdown;
