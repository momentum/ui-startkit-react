import * as React from 'react';
import * as screenfull from 'screenfull';
import { Icon } from 'components';

interface Props {
	title?: string;
	iconOff?: string;
	iconOn?: string;
	className?: string;
}

export default function FullScreen({
	iconOff = 'compress',
	iconOn = 'expand',
	title = 'Toggle Fullscreen',
	className,
}: Props): JSX.Element  {

	let anchorRef;

	function getIcon (isFullscreen: boolean = false) {
		return <Icon name={isFullscreen ? iconOff : iconOn} aria-hidden="true" />;
	}

	function handleClick (e: any) {
		e.preventDefault();
		screenfull.toggle();
		// Switch icon indicator
		let icons = anchorRef.getElementsByTagName('i')[0].classList;
		console.log(icons);
		if (screenfull.isFullscreen) {
			icons.remove(iconOn);
			icons.add(iconOff);
		} else {
			icons.remove(iconOff);
			icons.add(iconOn);
		}
	}

	if (screenfull.enabled) {
		return (
			<span onClick={ handleClick } ref={ el => anchorRef = el } title={title} className={className}>
				{ getIcon() }
			</span>
		);
	}

	return getIcon();
}

