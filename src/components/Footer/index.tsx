import * as React from 'react';
import './index.scss';

interface Props {
	children?: JSX.Element;
}

export default function Footer (props: Props): JSX.Element {
	const { children } = props;

	return (
		<footer id="layout-footer">
			{children}
		</footer>
	);
}

