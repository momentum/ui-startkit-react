import * as React from 'react';
import './index.scss';

interface CellProps {
	id: string;
	label: string;
	className: string;
	value: string;
}

interface RowProps {
	row?: CellProps;
}

interface Props {
	children?: JSX.Element;
	rows?: RowProps[];
	align?: string;
}

export default function InfoPanelBody ({ children, rows, align = 'right' }: Props ): JSX.Element {
	const bodyClassnames = [
		'info-panel-body',
		align === 'right' && `info-panel-body-aling-${align}`,
	].filter(Boolean).join(' ');

	return (
		<div className={bodyClassnames}>
			{ React.isValidElement(children)
				? children
				: renderRows()
			}
		</div>
	);

	function renderRows() {
		return rows.map((row: any, rowIndex: number) =>
			<div key={`info-panel-body__row-${rowIndex}`} className="info-panel-body__row">
				{row.map(({id, label, className = '', value}, cellIndex) =>
					<span key={`info-panel-body__cell-${cellIndex}`} className={`info-panel-body__row-cell ${className}`.trim()}>
						<label className="info-panel-body__row-cell__label">
							{label}
						</label>
						<span id={id} className="info-panel-body__row-cell__value">
							{value}
						</span>
					</span>,
				)}
			</div>,
		);
	}
}
