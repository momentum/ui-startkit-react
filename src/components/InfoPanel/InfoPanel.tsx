import * as React from 'react';
import './index.scss';
import * as Panel from 'components/InfoPanel';

interface Props {
	header?: any;
	body?: any;
}

export default function InfoPanel ({ header, body }: Props ): JSX.Element {
	return (
		<div className="info-panel">
			<Panel.Header {...header} />
			<Panel.Body {...body} />
		</div>
	);
}
