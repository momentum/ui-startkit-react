import * as React from 'react';
import * as Renderer from 'react-test-renderer';
import { shallow, configure } from 'enzyme';
import  Adapter from 'enzyme-adapter-react-16';
import InfoPanelHeader from '../info-panel-header';

configure({ adapter: new Adapter() })

const defaultProps = {
	id: 'jest-info-panel',
	title: 'Info Panel Title',
	label: 'Active',
	actions: [
		{
			id:'create',
			title: 'Create',
			isAllowed: true,
			onClick: jest.fn(),
		},
		{
			id:'edit',
			title: 'Edit',
			isAllowed: true,
			onClick: jest.fn(),
		},
	],
};

function setup (props, test, isShallow) {
	const fn = isShallow ? shallow : Renderer.create;
	const component = fn(<InfoPanelHeader {...props} />);
	return () => test(component);
}

describe('InfoPanelHeader Unit Tests', () => {
	it('Should exist', () => {
		expect(InfoPanelHeader).not.toBeNull();
	});

	it('Should render properly if contains properties', setup(
		defaultProps,
		(component) => expect(component).toMatchSnapshot()
	));

	describe('Should render the correct elements if props are provided', setup(
		defaultProps,
		(component) => {
			const header = component.find('.info-panel-header');
			const headerTitle = header.find('.info-panel-header__title');
			const headerLabel = header.find('.info-panel-header__label');
			const headerActions = header.find('.info-panel-header__actions').children();

			it('Should load panel header', () => {
				expect(header.length).toBe(1);
			});

			it('Should load panel header title', () => {
				expect(headerTitle.length).toBe(1);
			});

			it('Should load panel header label', () => {
				expect(headerLabel.length).toBe(1);
			});

			it(`Should have ${headerActions.length} action(s)`, () => {
				expect(headerActions.length).toBe(defaultProps.actions.length);
			});
		},
		true
	));
});
