import * as React from 'react';
import * as Renderer from 'react-test-renderer';
import { shallow, configure } from 'enzyme';
import  Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() })

import InfoPanelBody from '../info-panel-body';

const defaultProps = {
	children: [
		[
			{ id: 'firstName', label: 'First Name', value: 'ITN' },
			{ id: 'lastName', label: 'Last Name', value: 'User' },
			{ id: 'email', label: 'Email', value: 'user@itntworks.com' },
		],
	],
};

function setup (props, test, isShallow) {
	const fn = isShallow ? shallow : Renderer.create;
	const component = fn(<InfoPanelBody {...props} />);
	return () => test(component);
}

describe('InfoPanelBody Unit Tests', () => {
	it('Should exist', () => {
		expect(InfoPanelBody).not.toBeNull();
	});

	it('Should NOT render if it does NOT contains any children', setup(
		{},
		(component) => expect(component.toJSON()).toBe(null)
	));

	it('Should render if it contain children prop', setup(
		defaultProps,
		(component) => expect(component).toMatchSnapshot()
	));

	describe('Should render if does have valid children', setup(
		defaultProps,
		(component) => {
			const panelBody = component.find('.info-panel-body');
			const panelRows = panelBody.find('.info-panel-body__row');
			const panelRowCells = panelRows.find('.info-panel-body__row-cell');
			const expectedTotalRows = defaultProps.children.length;
			const expectedTotalCells = defaultProps.children[0].length;

			it('Should load panel body', () => {
				expect(panelBody.length).toBe(1);
			});

			it(`Should have ${expectedTotalRows} info row(s)`, () => {
				expect(panelRows.length).toBe(expectedTotalRows);
			});

			it(`Should have ${expectedTotalCells} info cell(s) in the first row`, () => {
				expect(panelRowCells.length).toBe(expectedTotalCells);
			});
		},
		true
	));
});
