import * as React from 'react';
import * as Renderer from 'react-test-renderer';
import { shallow, configure } from 'enzyme';
import  Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() })
