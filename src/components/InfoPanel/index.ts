export { default as InfoPanel } from './InfoPanel';
export { default as Header } from './InfoPanel.Header';
export { default as Body } from './InfoPanel.Body';
