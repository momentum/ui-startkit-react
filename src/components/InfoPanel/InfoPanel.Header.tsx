import * as React from 'react';
import { kebabCase } from 'lodash';
import { Icon } from 'components';
import './index.scss';

interface LabelProps {
	id: string;
	className: string;
	name: string;
}

interface ActionProps {
	id: string;
	icon: string;
	role: string;
	disabled: boolean;
	title: string;
	onClick: () => {};
	isAllowed: () => {};
}

interface Props {
	id: string;
	className?: string;
	title: any;
	children?: JSX.Element;
	label?: string | LabelProps;
	actions?: ActionProps[];
}

export default function InfoPanelHeader ({
	id,
	className,
	title,
	children,
	label,
	actions = [],
}: Props ): JSX.Element {
	const updatedClassnames = [
		'info-panel-header',
		className,
	].filter(Boolean).join(' ');

	function buildActions() {
		return actions
			.filter(({isAllowed = true}) => isAllowed)
			.map(({id, icon, title, role, disabled, onClick}) => (
				<button key={id || title} id={id} onClick={onClick} role={role} disabled={disabled}>
					{ icon && <Icon icon={icon} /> }
					{ title }
				</button>
			));
	}

	// eslint-disable-next-line react/no-multi-comp
	function buildSubTitle() {
		const { name, id = kebabCase(name), className = '' } = typeof label === 'object'
			? label
			: { name: label };

		return (
			<span id={id || kebabCase(name) } className={`info-panel-header__label ${className}`.trim()}>
				{name}
			</span>
		);
	}

	return (
		<div className={updatedClassnames}>
			<span id={id || kebabCase(title)} className="info-panel-header__title">{title}</span>
			{ !label
				? null
				: React.isValidElement(label)
					? label
					: buildSubTitle()
			}
			{ !actions.length
				? null
				: <div className="info-panel-header__actions">{ buildActions() }</div>
			}
			{ children }
		</div>
	);
}

