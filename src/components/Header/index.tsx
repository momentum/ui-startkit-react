import * as React from 'react';
import Menu from 'components/Menu';
import Logo from 'components/Logo';
import IMenuItem from 'core/interfaces/IMenuItem';

import './index.scss';

interface Props {
	user?: any;
	collapsed?: boolean;
	title?: string;
	icon?: string;
	showLogo: boolean;
	routes?: Array<IMenuItem>;
	activeRoute?: string;
	onLogout: () => void;
	onTogglePanel: (side: string) => void;
	onRouteChange?: (to: string) => void;
}

export default function Header({
	collapsed,
	title,
	user = {},
	icon,
	showLogo,
	routes,
	activeRoute,
	onTogglePanel,
	onLogout,
	onRouteChange,
}: Props): JSX.Element {
	const classNames = [
		'header',
		collapsed && 'collapsed',
	].filter(Boolean).join(' ');

	// const trigger = (
	// 	<div className="profile">
	// 		<div className="photo">
	// 			{/*<Image avatar src={`https://avatars.io/twitter/${user.username}`} /> */}
	// 		</div>
	// 		<div className="info">
	// 			<span className="greetings">Hello,</span>
	// 			<span className="name">{ user.firstName || '...' }</span>
	// 		</div>
	// 	</div>
	// );

	// function handleProfileChange({ target: { value } }) {
	// 	const { action } = options.find(({ key }) => key === value);
	// 	action();
	// }

	const profileRoutes = [
		{
			id: 'profile',
			label: user.firstName,
			href: '',
			icon: 'account_circle',
			order: 1,
			children: [
				{
					id: 'user-profile',
					label: 'My Account',
					icon: 'account_box',
					order: 1,
					href: '/user-profile',
				},
				{
					id: 'logout',
					label: 'Logout',
					icon: 'power_settings_new',
					order: 2,
					href: onLogout,
				},
			],
		},
	];

	return (
		<header id="header" className={classNames}>
			{showLogo && <div className="header-left">
				<Logo title={title} icon={icon} onClick={() => onRouteChange('/')}/>
			</div>}
			<div className="header-central">
				{ routes &&
				<Menu items={routes} position="horizontal" activeItem={activeRoute} onRouteChange={onRouteChange}/>
				}
			</div>
			<div className="header-right">
				<Menu items={profileRoutes} position="horizontal" onRouteChange={onRouteChange}/>
			</div>
		</header>
	);
}
