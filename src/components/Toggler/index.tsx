import * as React from 'react';
import './index.scss';

interface Props {
	id?: string;
	label?: string;
	checked?: boolean;
	aligment?: string;
	type?: string;
	onChange: (event: any) => void;
}

export default function Toggler ({
	id,
	label,
	checked,
	onChange,
	aligment = 'right',
	type = 'flat',
}: Props): JSX.Element {
	return (
		<div className="tgl-item">
			<div className="tgl-button">
				<input type="checkbox" className={`tgl ${type} ${aligment}`} id={id} onChange={onChange} checked={checked} />
				<label htmlFor={id} className="tgl-btn"></label>
			</div>
			<div className="tgl-label">
				{ label }
			</div>
		</div>
	);
}
