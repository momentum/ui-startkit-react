import NotebookPage from './containers';

const key = 'notebook';
const path = '/notebook';

export default {
	key,
	path,
	component: NotebookPage,
	routes: [
		{ path: `${path}/:id?/:action?`, component: NotebookPage },
	],
	data: {
		icon: 'place',
		title: 'My Notebook',
		description: 'Notebook',
	},
	navigation: {
		label: 'Notebook',
		order: 4,
	},
};
