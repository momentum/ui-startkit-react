import { DataStore } from 'core/data';
import Notebook from './models/Notebook';
import RealTime from 'core/realTime';

const dataStore = DataStore<Notebook>({
	endpoint: 'notebook',
	store: 'notebook',
	key: 'id',
	cache: true,
	onRealtimeUpdate: () => {
		const realTime = RealTime.getInstance();
		// console.info('Notebook Listeners activated');
		realTime.on('Notebook', 'Create', (model) => dataStore.updateStore(model, 'add') );
		realTime.on('Notebook', 'Update', (model) => dataStore.updateStore(model, 'update'));
		realTime.on('Notebook', 'Delete', (model) => dataStore.updateStore(model, 'delete'));
	},
	// transformResponse: (response) => {
	// 	return response.reduce((result, model) => {
	// 		result.push({
	// 			...model,
	// 			created: model.created.substr(0, 10),
	// 			updated: model.updated.substr(0, 10),
	// 		});
	// 		return result;
	// 	}, []);
	// },
});

export default dataStore;
