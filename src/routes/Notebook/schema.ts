import definitions from 'routes/schemas.definitions';

export default {
	type: 'object',
	properties: {
		id: { type: 'number' },
		title: { $ref: '#/definitions/hasString' },
		description: { $ref: '#/definitions/hasString' },
		meta: { type: 'string' },
		sortOrder: { type: 'number' },
		starred: {  type: 'boolean' },
	},
	required: [
		'title',
		'description',
		'sortOrder',
	],
	definitions,
};

