import * as React from 'react';
import * as Panel from 'components/Panel';
import { autobind } from 'core-decorators';

import Notebook from '../models/Notebook';
import { InfoPanel } from 'components/InfoPanel';

interface Props extends React.HTMLProps<HTMLDivElement> {
	model: Notebook;
	isLoading?: boolean;
	onEdit: (item: Notebook) => void;
}

@autobind
export default class NotebookDetails extends React.Component<Props> {
	constructor (props: Props) {
		super(props);
	}

	render() : JSX.Element {
		const { model = new Notebook(), onEdit} = this.props;

		return (
			<Panel.Element
				actions={[
					{ id: 'edit', icon: 'mode_edit', disabled: false, label: 'Edit', onClick: onEdit },
				]}>
				<InfoPanel
					header={{ id: 'notebook-info', title: model.title }}
					body={{
						rows: [
							[
								{ id: 'id', label: 'ID', value: model.id},
								{ id: 'title', label: 'Name', value: model.title},
								{ id: 'description', label: 'Description', value: model.description},
								{ id: 'meta', label: 'Meta', value: model.meta},
								{ id: 'starred', label: 'Starred', value: model.starred},
								{ id: 'sortOrder', label: 'Order', value: model.sortOrder},
								{ id: 'created', label: 'Created', value: model.created},
								{ id: 'updated', label: 'Updated', value: model.updated},
							],
						],
					}}
				/>
			</Panel.Element>
		);
	}
}
