import * as React from 'react';
import * as Panel from 'components/Panel';
import * as ListView from 'components/ListView';

import Notebook from '../models/Notebook';

interface Props extends ListView.Props<Notebook> {}
interface State extends ListView.State<Notebook> {}

export default class NotebookList extends ListView.Master<Notebook, Props, State> {
	constructor (props: Props) {
		super(props);

		this.domain = Notebook.domain;
		this.key = Notebook.key;
		this.filterKeys = ['title', 'description', 'meta'];

		this.state = {};
	}

	render(): JSX.Element {
		const { models, checkedModels, onCheck, onDelete, onCreate, onGet } = this.props;

		const actions: Panel.Action[] = [
			{ id: 'check-all', icon: 'done_all', disabled: false, iconOnly: true, label: 'Check All', onClick: () => onCheck(models.map((item) => item[Notebook.key]), true) },
			{ id: 'delete', icon: 'delete', iconOnly: true, disabled: !checkedModels.length, label: 'Delete', onClick: onDelete, confirmationMessage: 'Are you sure you want to delete this item?' },
			{ id: 'create', icon: 'add', iconOnly: true, disabled: false, label: 'Create', onClick: onCreate },
			{ id: 'refresh', icon: 'refresh', iconOnly: true, disabled: false, label: 'Refresh', onClick: () => onGet(false) },
		];

		const filters: Panel.Filter[] = [
			{
				id: 'keyword-filter',
				type: 'text',
				placeholder: 'Filter notes',
				onChange: ({target}) => this.setState({keyword: target.value}),
			},
		];

		return (
			<Panel.Element actions={actions} filters={filters}>
				{ super.render() }
			</Panel.Element>
		);
	}

	renderListModel({id, title}: Notebook): JSX.Element  {
		return (
			<div key={id}>
				<div className="font-small">{id}</div>
				<div className="font-bold">{title}</div>
			</div>
		);
	}
}
