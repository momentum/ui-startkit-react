import * as React from 'react';
import { autobind } from 'core-decorators';

import * as Panel from 'components/Panel';
import * as Form from 'components/Form';

import Notebook from '../models/Notebook';
import schema from '../schema';

interface Props extends Form.Props<Notebook> {}
interface State extends Form.State<Notebook> {}

@autobind
class NotebookForm extends Form.Master<Notebook, Props, State> {
	constructor (props: Props, context) {
		super(props, context);

		this.domain = Notebook.domain;
		this.key = Notebook.key;
		this.schema = schema;
		this.id = `form-${Notebook.domain}`;

		this.state = {
			model: props.model,
			fields: [],
		};
	}

	componentWillReceiveProps(nextProps: Props) {
		if (!nextProps.isSaving) {
			this.setState(this.getPartialState(nextProps));
		}
	}

	componentDidMount() {
		const state = this.getPartialState();
		this.setState(state, super.componentDidMount);
	}

	render(): JSX.Element {
		const { isSaving, onCancel } = this.props;
		const actions: Panel.Action[] = [
			{ id: 'cancel', icon: 'clear', disabled: isSaving, label: 'Cancel', onClick: onCancel },
			{ id: 'save', icon: 'save', disabled: isSaving, label: 'Save', onClick: this.handleSave },
		];
		return (
			<Panel.Element actions={actions}>
				{ super.render() }
			</Panel.Element>
		);
	}

	getPartialState(nextProps = null): State {
		const { model } = nextProps || this.props;
		const fields: Form.Field[] = [
			{ id: 'id', name: 'id', type: 'hidden', label: 'Id', value: model.id},
			{ id: 'title', name: 'title', type: 'text', label: 'Title', value: model.title},
			{ id: 'description', name: 'description', type: 'textarea', label: 'Description', value: model.description},
			{ id: 'meta', name: 'meta', type: 'text', label: 'Meta', value: model.meta},
			{ id: 'sortOrder', name: 'sortOrder', type: 'number', label: 'Sort Order', value: model.sortOrder},
			{ id: 'starred', name: 'starred', type: 'checkbox', label: 'Starred', value: model.starred},
		];

		return { model, fields };
	}
}

export default NotebookForm;
