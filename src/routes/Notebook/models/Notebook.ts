export default class Notebook  {
	id: number;
	title: string = '';
	description: string = '';
	meta: string = '';
	starred: boolean = false;
	created: string = '';
	updated: string = '';
	sortOrder: number = 0;

	static domain = 'notebook';
	static key = 'id';
}
