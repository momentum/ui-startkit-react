import * as React from 'react';
import { autobind } from 'core-decorators';
import { withRouter } from 'react-router';
import { branch } from 'baobab-react/higher-order';
import { animateTransition } from 'core/decorators';
import Logger from 'core/logger';

import * as Container from 'components/Container';
import * as Panel from 'components/Panel';
import * as Component  from '../components';

import Actions from '../Actions';
import Notebook from '../models/Notebook';

interface Props extends Container.Props<Notebook, Container.Params> {}
interface State extends Container.State<Notebook> {}

/**
 * Baobab cursors used by notebook domain
 */
@branch({
	isLoading: ['ajax', 'loading'],
	isSaving: ['ajax', 'saving'],
	models: ['notebook', 'models'],
	selectedModel: ['notebook', 'selectedModel'],
	checkedModels: ['notebook', 'checkedModels'],
})
@autobind
class NotebookPage extends Container.Master<Notebook, Props, State>  {
	constructor(props: Props) {
		super(props);

		this.domain = Notebook.domain;
		this.key = Notebook.key;
		this.toggleSelection = Actions.toggleSelection;

		this.state = {};
	}

	componentDidMount() {
		Actions.activateListeners();
	}

	componentWillUnmount() {
		Actions.clearSelection();
	}

	/**
	 * Render the Notebook container
	 * @returns {JSX.Element}
	 * @memberof NotebookPage
	 */
	render(): JSX.Element {
		const { isLoading, isSaving, models = [], checkedModels, selectedModel } = this.props;
		const { editingModel } = this.state;

		return (
			<Panel.Container>
				<Component.List
					models={models}
					isLoading={isLoading}
					checkedModels={checkedModels}
					selectedModel={selectedModel}
					onCheck={Actions.toggleCheck}
					onGet={Actions.get}
					onCreate={() => this.handleAction(new Notebook(), 'new')}
					onDelete={async () => {
						const response = await Actions.remove(checkedModels);
						if (response.message) {
							Logger.info(response.message);
						}
					}}
					onSelect={(item) => this.handleAction(item)}
				/>

				<Panel.Splitter propagate={true}/>

				{!editingModel &&
				<Component.Details
					model={selectedModel}
					isLoading={isLoading}
					onEdit={() => this.handleAction(selectedModel, 'edit')}
				/>}

				{editingModel &&
				<Component.Form
					model={editingModel}
					isSaving={isSaving}
					onCancel={() => this.handleAction(selectedModel)}
					onSubmit={(data) => Actions.save(data, data[Notebook.key])}
				/>}
			</Panel.Container>
		);
	}
}

const RoutedComponent = withRouter(NotebookPage);

export default animateTransition()(RoutedComponent);
