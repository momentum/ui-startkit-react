export default {
	boolean: {
		type: 'boolean',
	},
	positiveInteger: {
		type: 'integer',
		minimum: 0,
		maximum: 9999999,
	},
	positiveNumber: {
		type: 'number',
		minimum: 0.0,
		maximum: 999999999999.0,
	},
	positiveDecimalsLessThanOne: {
		type: 'number',
		minimum: 0.00,
		maximum: 1.00,
	},
	allocations: {
		type: 'array',
		items: { $ref: '#/definitions/positiveInteger' },
	},
	id: {
		type: 'integer',
		minimum: 1,
	},
	ids: {
		type: 'array',
		items: { $ref: '#/definitions/id' },
	},
	requiredIds: {
		type: 'array',
		items: { type: 'integer', minimum: 1 },
		minItems: 1,
	},
	date: {
		type: 'string',
		pattern: '^[0-9]{4}-[0-9]{2}-[0-9]{2}',
	},
	time: {
		description: 'UTC time of day for start time',
		type: 'string',
		pattern: '^(([0-9]?|[1]{1}[0-9]{1}|[2]{1}[0-3]{1}|0[0-9]{1}):[0-5]{1}[0-9]{1}(:[0-5][0-9])?)$|^24:00$|^24:00:00$',
	},
	requiredTime: {
		description: 'UTC time of day for start time',
		type: 'string',
		pattern: '^(([0-9]?|[1]{1}[0-9]{1}|[2]{1}[0-3]{1}|0[0-9]{1}):[0-5]{1}[0-9]{1}(:[0-5][0-9])?)$|^24:00$|^24:00:00$',
		minimum: 4,
	},
	quarter: {
		type: 'string',
		pattern: '^q[0-9]{1}$',
	},
	year: {
		type: 'integer',
		pattern: '^[0-9]{4}$',
	},
	quarterYearFull: {
		type: 'string',
		pattern: '^q[0-9]{1}-[0-9]{4}$',
	},
	quarterYearArray: {
		type: ['array'],
		items: {
			type: 'object',
			properties: {
				quarter: { $ref: '#/definitions/quarter' },
				year: { $ref: '#/definitions/year' },
			},
		},
		minItems: 1,
	},
	email: {
		type: 'string',
		pattern: '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$',
	},
	optionalEmail: {
		type: ['string', 'null'],
		pattern: '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$',
	},
	code: {
		type: 'string',
		minLength: 1,
		maxLength: 8,
	},
	programCode: {
		type: 'string',
		minLength: 1,
		maxLength: 20,
	},
	guid: {
		type: 'string',
		pattern: '^[{|\\(]?[0-9A-Fa-f]{8}[-]?([0-9A-Fa-f]{4}[-]?){3}[0-9A-Fa-f]{12}[\\)|}]?$',
	},
	hasString: {
		type: 'string',
		minLength: 1,
	},
	hasInteger: {
		type: 'integer',
		minLength: 1,
	},
	quarterWeeks: {
		type: 'array',
		items: {
			type: 'object',
			properties: {
				quarter: {
					type: 'string',
					pattern: '^q[0-9]$',
				},
				year: {
					type: 'integer',
					pattern: '^[0-9]{4}$',
				},
				weeks: {
					type: 'integer',
				},
				startDate: {
					type: 'string',
					pattern: '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
				},
			},
		},
	},
	address: {
		type: ['object'],
		properties: {
			address1: { $ref: '#/definitions/hasString' },
			address2: { type: ['string', 'null'] },
			city: { $ref: '#/definitions/hasString' },
			state: { $ref: '#/definitions/hasString' },
			country: { type: 'string' },
			zip: { type: ['string', 'integer'] },
		},
		required: ['address1', 'city', 'state', 'country', 'zip'],
	},
	phone: {
		type: ['object'],
		properties: {
			number: { type: ['string', 'integer'] },
			extension: { type: ['string', 'null', 'integer'] },
		},
		required: ['number'],
	},
	contact: {
		type: ['object', 'null'],
		properties: {
			id: { $ref: '#/definitions/id' },
			firstName: { $ref: '#/definitions/hasString' },
			lastName: { $ref: '#/definitions/hasString' },
			email: { $ref: '#/definitions/email' },
			title: { $ref: '#/definitions/hasString' },
			phone: { $ref: '#/definitions/phone' },
			isPrimary: { type: 'boolean' },
		},
		required: ['firstName', 'lastName', 'email', 'title', 'phone'],
	},
};
