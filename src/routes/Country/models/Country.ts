export default class Country  {
	iso2: string = '';
	iso3: string = '';
	name: string = '';
	currency: string = '';
	phoneCode: string = '';
	longitude: string = '';
	latitude: string = '';
	isNew: boolean = false;

	static domain = 'country';
	static key = 'iso2';

	constructor(isNew: boolean = false) {
		this.isNew = isNew;
	}
}
