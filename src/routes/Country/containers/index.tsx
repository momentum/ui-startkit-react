import * as React from 'react';
import { autobind } from 'core-decorators';
import { withRouter } from 'react-router';
import { branch } from 'baobab-react/higher-order';
import { animateTransition } from 'core/decorators';

import * as Container from 'components/Container';
import * as Panel from 'components/Panel';
import * as Component  from '../components';

import Actions from '../Actions';
import Country from '../models/Country';

interface Props extends Container.Props<Country, Container.Params> {}
interface State extends Container.State<Country> {}

/**
 * Baobab cursors used by country domain
 */
@branch({
	isLoading: ['ajax', 'loading'],
	isSaving: ['ajax', 'saving'],
	models: ['country', 'models'],
	selectedModel: ['country', 'selectedModel'],
	checkedModels: ['country', 'checkedModels'],
})
@autobind
class CountryPage extends Container.Master<Country, Props, State>  {
	constructor(props: Props) {
		super(props);

		this.domain = Country.domain;
		this.key = Country.key;
		this.toggleSelection = Actions.toggleSelection;

		this.state = {};
	}

	componentWillUnmount() {
		Actions.clearSelection();
	}

	/**
	 * Render the Country container
	 * @returns {JSX.Element}
	 * @memberof CountryPage
	 */
	render(): JSX.Element {
		const { isLoading, isSaving, models = [], checkedModels, selectedModel } = this.props;
		const { editingModel } = this.state;

		return (
			<Panel.Container>
				<Component.List
					models={models}
					isLoading={isLoading}
					checkedModels={checkedModels}
					selectedModel={selectedModel}
					onCheck={Actions.toggleCheck}
					onGet={Actions.get}
					onCreate={() => this.handleAction(new Country(true), 'new')}
					onDelete={() => Actions.remove(checkedModels)}
					onSelect={(item) => this.handleAction(item)}
				/>

				<Panel.Splitter propagate={true}/>

				{!editingModel &&
				<Component.Details
					model={selectedModel}
					isLoading={isLoading}
					onEdit={() => this.handleAction(selectedModel, 'edit')}
				/>}

				{editingModel &&
				<Component.Form
					model={editingModel}
					isSaving={isSaving}
					onCancel={() => this.handleAction(selectedModel)}
					onSubmit={(data) => Actions.save(data, this.isNew(data))}
				/>}
			</Panel.Container>
		);
	}
}

const RoutedComponent = withRouter(CountryPage);

export default animateTransition()(RoutedComponent);
