import definitions from 'routes/schemas.definitions';

export default {
	type: 'object',
	properties: {
		iso2: { $ref: '#/definitions/hasString' },
		iso3: { $ref: '#/definitions/hasString' },
		name: { $ref: '#/definitions/hasString' },
		currency: { $ref: '#/definitions/hasString' },
		phoneCode: { $ref: '#/definitions/hasString' },
		longitude: { $ref: '#/definitions/hasString' },
		latitude: { $ref: '#/definitions/hasString' },
	},
	required: [
		'iso2',
		'iso3',
		'name',
		'currency',
		'phoneCode',
		'longitude',
		'latitude',
	],
	definitions,
};

