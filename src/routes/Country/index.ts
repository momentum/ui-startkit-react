import CountryPage from './containers';

const key = 'country';
const path = '/countries';

export default {
	key,
	path,
	component: CountryPage,
	routes: [
		{ path: `${path}/:id?/:action?`, component: CountryPage },
	],
	data: {
		icon: 'place',
		title: 'Countries',
		description: 'World Countries',
	},
	navigation: {
		parent: {
			label: 'Location',
			icon: 'map',
			order: 3,
		},
		label: 'Countries',
		order: 1,
	},
};
