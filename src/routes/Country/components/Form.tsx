import * as React from 'react';
import { autobind } from 'core-decorators';

import * as Panel from 'components/Panel';
import * as Form from 'components/Form';

import Country from '../models/Country';
import schema from '../schema';

interface Props extends Form.Props<Country> {}
interface State extends Form.State<Country> {}

@autobind
class CountryForm extends Form.Master<Country, Props, State> {
	currencies: Form.FieldOptions[] = [
		{ label: '- Select -', value: ''},
		{ label: 'USD', value: 'USD'},
		{ label: 'CAD', value: 'CAD'},
	];

	constructor (props: Props, context) {
		super(props, context);

		this.domain = Country.domain;
		this.key = Country.key;
		this.schema = schema;
		this.id = `form-${Country.domain}`;
	}

	componentDidMount() {
		const { model } = this.props;

		const fields: Form.Field[] = [
			{ id: 'iso2', name: 'iso2', type: 'text', label: 'Iso 2', value: model.iso2, maxLength: 2, disabled: model.isNew},
			{ id: 'iso3', name: 'iso3', type: 'text', label: 'Iso 3', value: model.iso3, maxLength: 3, disabled: model.isNew},
			{ id: 'name', name: 'name', type: 'text', label: 'Name', value: model.name},
			{ id: 'currency', name: 'currency', type: 'select', label: 'Currency', value: model.currency, options: this.currencies},
			{ id: 'phoneCode', name: 'phoneCode', type: 'text', label: 'Phone Code', value: model.phoneCode},
			{ id: 'longitude', name: 'longitude', type: 'text', label: 'Longitude', value: model.longitude},
			{ id: 'latitude', name: 'latitude', type: 'text', label: 'Latitude', value: model.latitude},
		];

		this.setState({ model, fields }, super.componentDidMount);
	}

	render(): JSX.Element {
		const { isSaving, onCancel } = this.props;
		const actions: Panel.Action[] = [
			{ id: 'cancel', icon: 'clear', disabled: isSaving, label: 'Cancel', onClick: onCancel },
			{ id: 'save', icon: 'save', disabled: isSaving, label: 'Save', onClick: this.handleSave },
		];

		return (
			<Panel.Element actions={actions}>
				{ super.render() }
			</Panel.Element>
		);
	}
}

export default CountryForm;
