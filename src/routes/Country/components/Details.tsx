import * as React from 'react';
import * as Panel from 'components/Panel';
import { autobind } from 'core-decorators';

import Country from '../models/Country';
import { InfoPanel } from 'components/InfoPanel';

interface Props extends React.HTMLProps<HTMLDivElement> {
	model: Country;
	isLoading?: boolean;
	onEdit: (item: Country) => void;
}

@autobind
export default class CountryDetails extends React.Component<Props> {
	constructor (props: Props) {
		super(props);
	}

	render() : JSX.Element {
		const { model = new Country(), onEdit} = this.props;

		return (
			<Panel.Element
				actions={[
					{ id: 'edit', icon: 'mode_edit', disabled: false, label: 'Edit', onClick: onEdit },
				]}>
				<InfoPanel
					header={{ id: 'country-info', title: model.name }}
					body={{
						rows: [
							[
								{ id: 'iso2', label: 'Iso 2', value: model.iso2},
								{ id: 'iso3', label: 'Iso 3', value: model.iso3},
								{ id: 'name', label: 'Name', value: model.name},
								{ id: 'currency', label: 'Currency', value: model.currency},
								{ id: 'phoneCode', label: 'Phone Code', value: model.phoneCode},
								{ id: 'coordinates', label: 'Coordinates', value: `${model.longitude}, ${model.latitude}`},
							],
						],
					}}
				/>
			</Panel.Element>
		);
	}
}
