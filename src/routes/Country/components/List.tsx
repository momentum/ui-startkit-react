import * as React from 'react';
import * as Panel from 'components/Panel';
import * as ListView from 'components/ListView';

import Country from '../models/Country';

interface Props extends ListView.Props<Country> {}
interface State extends ListView.State<Country> {}

export default class CountryList extends ListView.Master<Country, Props, State> {
	constructor (props: Props) {
		super(props);

		this.domain = Country.domain;
		this.key = Country.key;
		this.filterKeys = ['name', 'iso2', 'currency'];

		this.state = {};
	}

	render(): JSX.Element {
		const { models, checkedModels, onCheck, onDelete, onCreate } = this.props;

		const actions: Panel.Action[] = [
			{ id: 'check-all', icon: 'done_all', disabled: false, iconOnly: true, label: 'Check All', onClick: () => onCheck(models.map((item) => item[Country.key]), true) },
			{ id: 'delete', icon: 'delete', iconOnly: true, disabled: !checkedModels.length, label: 'Delete', onClick: onDelete },
			{ id: 'create', icon: 'add', iconOnly: true, disabled: false, label: 'Create', onClick: onCreate },
		];

		const filters: Panel.Filter[] = [
			{ id: 'keyword-filter', type: 'text', placeholder: 'Filter Countries', onChange: ({target}) => this.setState({keyword: target.value}) },
		];

		return (
			<Panel.Element actions={actions} filters={filters}>
				{ super.render() }
			</Panel.Element>
		);
	}

	renderListModel({iso2, name}: Country): JSX.Element  {
		return (
			<div key={iso2}>
				<div className="font-small">{iso2}</div>
				<div className="font-bold">{name}</div>
			</div>
		);
	}
}
