import { DataStore } from 'core/data';
import Country from './models/Country';

const dataStore = DataStore<Country>({
	endpoint: 'geo/countries',
	store: 'country',
	key: 'iso2',
	cache: true,
});

export default {
	...dataStore,
	async save (model: any, isNew: boolean) {
		return await dataStore.save(model, !isNew && model.code);
	},
};
