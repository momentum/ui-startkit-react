import DashboardPage from './containers';

export default {
	key: 'dashboard',
	path: '/',
	component: DashboardPage,
	exact: true,
	data: {
		icon: 'place',
		title: 'Dashboard',
		description: 'Dashboard',
		headerless: true,
	},
	navigation: {
		label: 'Dashboard',
		order: 1,
	},
};
