import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import View from '../components/View';
import { animateTransition } from 'core/decorators';

interface Props extends RouteComponentProps<{}> {
	children: JSX.Element |  JSX.Element[];
}

interface State {
	lookup: any;
}

@animateTransition()
export default class DashboardPage extends React.Component<Props, State>  {

	state: State = {
		lookup: {},
	};

	constructor(props: Props) {
		super(props);
	}

	render(): JSX.Element {
		return (
			<View
				header="Header"
				lookup={ this.state.lookup }
				description="This is the Dashboard page."
			/>
		);
	}
}
