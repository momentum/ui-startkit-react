import * as React from 'react';

import '../index.scss';

interface Props extends React.HTMLProps<HTMLDivElement> {
	lookup: any;
	header: string;
	description: string;
}

export default function DashboardView ({ lookup, header, description }: Props): JSX.Element {
	return (
		<div className="page-content" />
	);
}
