import AboutPage from './components';

export default {
	key: 'about',
	path: '/about',
	component: AboutPage,
	exact: true,
	data: {
		icon: 'info outline',
		title: 'About',
		description: 'About Page',
	},
};
