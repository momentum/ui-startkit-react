export default {
	type: 'object',
	properties: {
		iso2: { type: 'string'},
		code: { type: 'string'},
		name: { type: 'string'},
	},
	required: [
		'iso2',
		'code',
	],
};

