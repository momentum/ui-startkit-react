import ProvincePage from './containers';

const key = 'province';
const path = '/provinces';

export default {
	key,
	path,
	component: ProvincePage,
	routes: [
		{ path: `${path}/:id?/:action?`, component: ProvincePage },
	],
	data: {
		icon: 'terrain',
		title: 'Provinces',
		description: 'Provinces',
	},
	navigation: {
		parent: {
			label: 'Location',
			order: 3,
		},
		label: 'Provinces',
		order: 2,
	},
};
