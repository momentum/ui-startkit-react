export default class Province {
	public iso2: string;
	public code: string;
	public name: string;

	static domain = 'Province';
	static key = 'code';
}
