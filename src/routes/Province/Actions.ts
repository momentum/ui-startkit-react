import { IDataStore, DataStore } from 'core/data';
import Province from './models/Province';

const dataStore: IDataStore<Province> = DataStore<Province>({
	endpoint: 'geo/states',
	store: 'province',
	key: 'code',
	cache: true,
});

export default {
	...dataStore,
	async get (iso2: string, page: number = 1, size: number = 999) {
		return await dataStore.get({iso2, page, size});
	},
	async save (model: any, isNew: boolean) {
		return await dataStore.save(model, !isNew && model.code);
	},
};
