import * as React from 'react';
import { autobind } from 'core-decorators';
import { withRouter } from 'react-router';
import { branch } from 'baobab-react/higher-order';
import { animateTransition } from 'core/decorators';

import * as Container from 'components/Container';
import * as Panel from 'components/Panel';
import * as Component  from '../components';

import CountryActions from '../../Country/Actions';
import Actions from '../Actions';
import Province from '../models/Province';
import Country from '../../Country/models/Country';

interface Props extends Container.Props<Province, Container.Params> {
	parentModels?: Country[];
}
interface State extends Container.State<Province> {
	iso2: string;
}

/**
 * Baobab cursors used by province domain
 */
@branch({
	isLoading: ['ajax', 'loading'],
	isSaving: ['ajax', 'saving'],
	models: ['province', 'models'],
	parentModels: ['country', 'models'],
	selectedModel: ['province', 'selectedModel'],
	checkedModels: ['province', 'checkedModels'],
})
@autobind
class ProvincePage extends Container.Master<Province, Props, State>  {

	constructor(props: Props) {
		super(props);

		this.domain = Province.domain;
		this.key = Province.key;
		this.toggleSelection = Actions.toggleSelection;

		this.state = { iso2: 'ca' };
	}

	componentDidMount () {
		const { parentModels } = this.props;

		if (!parentModels.length) {
			CountryActions.get();
		}
	}

	componentWillUnmount() {
		Actions.clearSelection();
	}

	/**
	 * Render the Province container
	 * @returns {JSX.Element}
	 * @memberof ProvincePage
	 */
	render(): JSX.Element {
		const { isLoading, isSaving, models = [], checkedModels, selectedModel, parentModels = [] } = this.props;
		const { editingModel } = this.state;

		return (
			<Panel.Container>
				<Component.List
					models={models}
					parentModels={parentModels}
					isLoading={isLoading}
					checkedModels={checkedModels}
					selectedModel={selectedModel}
					onCheck={Actions.toggleCheck}
					onGet={() => Actions.get(this.state.iso2)}
					onCreate={() => this.handleAction(new Province(), 'new')}
					onDelete={() => Actions.remove(checkedModels)}
					onSelect={(item) => this.handleAction(item)}
				/>

				<Panel.Splitter/>

				{!editingModel &&
				<Component.Details
					model={selectedModel}
					isLoading={isLoading}
					onEdit={() => this.handleAction(selectedModel, 'edit')}
				/>}

				<Panel.Splitter/>

				{editingModel &&
				<Component.Form
					isSaving={isSaving}
					parentModels={parentModels}
					model={editingModel}
					onCancel={() => this.handleAction(selectedModel)}
					onSubmit={(data) => Actions.save(data, this.isNew(data))}
				/>}
			</Panel.Container>
		);
	}
}

const RoutedComponent = withRouter(ProvincePage);

export default animateTransition()(RoutedComponent);
