import * as React from 'react';
import { autobind } from 'core-decorators';

import * as Panel from 'components/Panel';
import * as Form from 'components/Form';

import Province from '../models/Province';
import Country from '../../Country/models/Country';
import schema from '../schema';

import '../index.scss';

interface Props extends Form.Props<Province> {
	parentModels: Country[];
}

interface State extends Form.State<Province> {}

@autobind
class ProvinceForm extends Form.Master<Province, Props, State> {
	constructor (props: Props, context) {
		super(props, context);

		this.schema = schema;
		this.id = `form-${Province.domain}`;
		this.className = 'form';
	}

	componentDidMount() {
		const { model, parentModels = [] } = this.props;
		const options: Form.FieldOptions[] = parentModels.map(({iso2, name}) => ({ value: iso2, label: name }));
		const fields: Form.Field[] = [
			{ id: 'iso2', name: 'iso2', type: 'select', label: 'Country', value: model.iso2, disabled: !!model.iso2, options},
			{ id: 'code', name: 'code', type: 'text', label: 'Code', value: model.code, disabled: !!model.code},
			{ id: 'name', name: 'name', type: 'text', label: 'Name', value: model.name},
		];

		this.setState({ model, fields }, super.componentDidMount);
	}

	render(): JSX.Element {
		const { isSaving, onCancel } = this.props;
		const actions: Panel.Action[] = [
			{ id: 'cancel', icon: 'clear', disabled: isSaving, label: 'Cancel', onClick: onCancel },
			{ id: 'save', icon: 'save', disabled: isSaving, label: 'Save', onClick: this.handleSave },
		];

		return (
			<Panel.Element actions={actions}>
				{ super.render() }
			</Panel.Element>
		);
	}
}

export default ProvinceForm;
