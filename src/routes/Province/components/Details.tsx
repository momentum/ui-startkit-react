import * as React from 'react';
import * as Panel from 'components/Panel';
import { autobind } from 'core-decorators';
import Province from '../models/Province';
import { InfoPanel } from 'components/InfoPanel';
import '../index.scss';

interface Props extends React.HTMLProps<HTMLDivElement> {
	model: Province;
	isLoading?: boolean;
	onEdit: (item: Province) => void;
}

@autobind
export default class ProvinceDetails extends React.Component<Props> {
	constructor (props: Props) {
		super(props);
	}

	render() : JSX.Element {
		const { model = new Province(), onEdit} = this.props;

		const actions = [
			{ id: 'edit', icon: 'mode_edit', disabled: false, label: 'Edit', onClick: onEdit },
		];

		return (
			<Panel.Element actions={actions}>
				<InfoPanel header={{ id: 'province-info', title: model.name }}
					body={{
						rows: [
							[
								{ id: 'iso2', label: 'Iso 2', value: model.iso2 },
								{ id: 'name', label: 'Name', value: model.name },
								{ id: 'code', label: 'Code', value: model.code },
							],
						],
					}}
				/>
			</Panel.Element>
		);
	}
}
