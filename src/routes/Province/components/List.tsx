import * as React from 'react';
import * as Panel from 'components/Panel';
import * as ListView from 'components/ListView';

import Province from '../models/Province';
import Country from '../../Country/models/Country';

interface Props extends ListView.Props<Province> {
	parentModels: Country[];
}

interface State extends ListView.State<Province> {}

export default class ProvinceList extends ListView.Master<Province, Props, State> {
	constructor (props: Props) {
		super(props);

		this.domain = Province.domain;
		this.key = Province.key;
		this.filterKeys = ['name', 'iso2'];

		this.state = {};
	}

	render() : JSX.Element {
		const { models, checkedModels, onCheck, onDelete, onCreate } = this.props;

		const actions: Panel.Action[] = [
			{ id: 'check-all', icon: 'done_all', disabled: false, label: 'Check All', onClick: () => onCheck(models.map((item) => item.iso2), true) },
			{ id: 'create', icon: 'add', disabled: false, label: 'Create', onClick: onCreate },
			{ id: 'delete', icon: 'delete', disabled: !checkedModels.length, label: 'Delete', onClick: onDelete },
		];

		const filters: Panel.Filter[] = [
			{ id: 'keyword-filter', type: 'text', placeholder: 'Filter Provinces', onChange: ({target}) => this.setState({keyword: target.value}) },
		];

		return (
			<Panel.Element actions={actions} filters={filters}>
				{ super.render() }
			</Panel.Element>
		);
	}

	renderListModel({iso2, code, name}: Province) {
		return (
			<div key={`${iso2}-${code}`}>
				<div className="font-small">{iso2}</div>
				<div className="font-bold">{name}, {code}</div>
			</div>
		);
	}
}
