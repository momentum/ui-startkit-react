import * as React from 'react';
// import { withRouter } from 'react-router';
import { kebabCase, sortBy } from 'lodash';
import { Route, Redirect } from 'react-router-dom';
import { Container } from 'layouts';
import settings from 'settings';
import { UserAuth } from 'core/auth';
import IMenuItem from 'core/interfaces/IMenuItem';

const appRoutes = {
	public: [
		'About',
	],
	account: [
		'Account/Login',
		'Account/ResetPassword',
		'Account/Signup',
	],
	protected: [
		'Account/UserProfile',
		'Country',
		'Province',
		'Notebook',
		'Dashboard',
	],
};

function navigationFactory (scope: string) {
	function getRoute ({ component: Component, ...props }) {
		function render  ({location}) {
			const state = { from: location };
			const isAuthenticated = UserAuth.isAuthenticated();

			switch (scope) {
				case 'account':
					return !isAuthenticated
						? <Component {...props} />
						: <Redirect to={{pathname: settings.defaultRoute, state}}/>;
				case 'protected':
					return isAuthenticated
						? <Component {...props} />
						: <Redirect to={{pathname: settings.loginRedirect, state}}/>;
				default:
					return <Component {...props} />;
			}
		}
		return <Route {...props} render={render} />;
	}

	const response = appRoutes[scope].reduce((result, path) => {
		const route = require('./' + path).default;

		if (route.routes) {
			route.routes.forEach((r, index) => result.routes.push(getRoute({...r, key: `${route.key}-${index}`})));
		}

		result.routes.push(getRoute(route));

		if (route.navigation) {
			const { navigation, key, path, data } = route;
			const parentLabel = (navigation.parent || {}).label;
			const parentNode = result.navigation.find(({label}) => label === parentLabel);
			const getItem = (id, href, label, icon, order): IMenuItem => ({ id, href, label, icon, order, children: [] });
			const menuItem: IMenuItem = parentNode
				? parentNode
				: navigation.parent
					? getItem(kebabCase(parentLabel), '', parentLabel, navigation.parent.icon, navigation.parent.order)
					: getItem(key, path, navigation.label, data.icon, navigation.order);

			if (parentLabel) {
				menuItem.children.push(getItem(key, path, navigation.label, data.icon, navigation.order));
			}

			if (!parentNode) {
				result.navigation.push(menuItem);
			}
		}
		return result;
	}, { routes: [], navigation: []});

	return response;
}

export default function () {
	const response: any = Object.keys(appRoutes).reduce((result, scope) => {
		const route = navigationFactory(scope);
		result.routes[scope] = route.routes;
		result.navigation[scope] = sortBy(route.navigation, 'order');
		return result;
	}, { routes: {}, navigation: {}});

	return <Container routes={response.routes} navigation={response.navigation} />;
}
