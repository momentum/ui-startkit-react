export default class Signup {
	username: string = '';
	password: string = '';
	confirmPassword: string = '';
	email: string = '';
	firstName: string = '';
	lastName: string = '';
	acceptTerms: boolean = false;

	static domain = 'signup';
	static key = 'username';
}
