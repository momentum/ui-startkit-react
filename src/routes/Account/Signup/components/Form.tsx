import * as React from 'react';
import { autobind } from 'core-decorators';
import { Link } from 'react-router-dom';

import * as Form from 'components/Form';

import Signup from '../models/Signup';
import schema from '../schema';

interface Props extends Form.Props<Signup> {}
interface State extends Form.State<Signup> {}

@autobind
class SignupForm extends Form.Master<Signup, Props, State> {
	constructor (props: Props, context) {
		super(props, context);

		this.domain = Signup.domain;
		this.key = Signup.key;
		this.schema = schema;
		this.id = `form-${Signup.domain}`;

		this.state = {
			fields: [],
			model: new Signup(),
		};
	}

	componentDidMount() {
		const { model, isSaving } = this.props;

		const fields: Form.Field[] = [
			{ id: 'username', name: 'username', type: 'text', label: 'Username' },
			{ id: 'email', name: 'email', type: 'text', label: 'Email' },
			{ id: 'password', name: 'password', type: 'password', label: 'Password' },
			{ id: 'confirmPassword', name: 'confirmPassword', type: 'password', label: 'Confirm Password' },
			{ id: 'firstName', name: 'firstName', type: 'text', label: 'First Name' },
			{ id: 'lastName', name: 'lastName', type: 'text', label: 'Last Name' },
			{ id: 'acceptTerms', name: 'acceptTerms', type: 'checkbox', label: 'Accept terms' },
		];

		const actions = [
			{ id: 'submit', icon: 'save', disabled: isSaving, label: 'Submit', onClick: this.handleSave },
		];

		this.setState({ model, fields, actions }, super.componentDidMount);
	}

	componentWillReceiveProps(nextProps, nextState) {
		this.setState({ errors: nextProps.errors  }, super.componentDidMount);
	}

	render(): JSX.Element {
		return (
			<div id="page-signup" className="page-account">
				{ super.render() }
				<div>
					<p>Already have an account? <Link to={'/login'}>Login to your account</Link>.</p>
				</div>
			</div>
		);
	}
}

export default SignupForm;
