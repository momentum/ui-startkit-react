import * as React from 'react';
import { autobind } from 'core-decorators';
import { withRouter } from 'react-router';
import { animateTransition } from 'core/decorators';
import { UserAction } from 'core/auth';
import { ValidationError } from 'core/exception';

import * as Container from 'components/Container';
import SignupForm from '../components/Form';

import Signup from '../models/Signup';

interface Props extends Container.Props<Signup, {}> {}
interface State extends Container.State<Signup> {
	isSaving?: boolean;
}

import '../../index.scss';

@autobind
class SignupPage extends Container.Master<Signup, Props, State>  {
	constructor(props: Props) {
		super(props);

		this.domain = Signup.domain;
		this.key = Signup.key;

		this.state = {
			editingModel: new Signup(),
		};
	}

	async handleSubmit (model) {
		try {
			this.setState({ isSaving: true });
			const response = await UserAction.signup(model);
			const errors: ValidationError = response.error
				? new ValidationError(response.error.message)
				: undefined;

			this.setState({ errors, isSaving: false }, () => {
				if (!response.error && response.redirect) {
					this.props.history.replace(response.redirect);
				}
			});
		} catch (e) {
			this.setState({ isSaving: false });
		}
	}

	render(): JSX.Element {
		const { editingModel, errors, isSaving } = this.state;

		return (
			<SignupForm
				model={editingModel}
				isSaving={isSaving}
				errors={errors}
				onSubmit={(model) => this.handleSubmit(model)}
			/>
		);
	}
}

const RoutedComponent = withRouter(SignupPage);

export default animateTransition(150)(RoutedComponent);
