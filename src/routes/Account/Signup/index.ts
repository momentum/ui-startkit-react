import SignupPage from './containers';

export default {
	key: 'signup',
	path: '/signup',
	component: SignupPage,
	exact: true,
	data: {
		icon: 'perm identity',
		title: 'Signup',
		description: 'Signup',
	},
};
