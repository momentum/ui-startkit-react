import definitions from 'routes/schemas.definitions';

export default {
	type: 'object',
	properties: {
		username: { $ref: '#/definitions/hasString' },
		password: { $ref: '#/definitions/hasString' },
		confirmPassword: { $ref: '#/definitions/hasString' },
		email: {
			type: 'string',
			pattern: '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$',
		},
		firstName: { $ref: '#/definitions/hasString' },
		lastName: { $ref: '#/definitions/hasString' },
		acceptTerms: { $ref: '#/definitions/boolean' },
	},
	required: ['username', 'email', 'firstName', 'password', 'confirmPassword'],
	definitions,
};
