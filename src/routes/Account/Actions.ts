import Api from 'core/api';
import { DataStore } from 'core/data';
import Login from './Login/models/Login';
import UserProfile from 'core/auth/UserProfile';

const dataStore = DataStore<Login>({
	endpoint: 'account',
	store: 'account',
	key: 'username',
});

export default {
	async loadProfile(): Promise<UserProfile> {
		const profile = await dataStore.find();
		return new UserProfile(profile);
	},

	async updateProfile(data: UserProfile): Promise<UserProfile> {
		const profile = await Api.patch('profile');
		return new UserProfile(profile);
	},

	async signup(data: any): Promise<any> {
		return await dataStore.save(data, 'account/register');
		// return new ActionResult(settings.defaultRoute, response.message);
	},

	async verifyAccount(data: any): Promise<any> {
		return await Api.patch('account/verifyAccount', data);
	},

	async changeEmail(data: any): Promise<any> {
		return await Api.patch('account/changeEmail', data);
	},

	async changeMobile(data: any): Promise<any> {
		return await Api.patch('account/changeMobile', data);
	},

	async changeAddress(data: any): Promise<any> {
		return await Api.patch('account/changeAddress', data);
	},

	async changePassword(data: any): Promise<any> {
		return await Api.patch('account/changePassword', data);
	},

	async isUsernameAvailable(username: string): Promise<any> {
		return await Api.get('account/isUsernameAvailable', { username });
	},

	async isEmailAvailable(email: string): Promise<any> {
		return await Api.get('account/isEmailAvailable', { email });
	},

	async isPhoneNumberAvailable(phoneNumber: string): Promise<any> {
		return await Api.get('account/isPhoneNumberAvailable', { phoneNumber });
	},

	async resetPassword(data: any): Promise<any> {
		return await Api.post('account/reset-password', data);
	},

	async requestUsernameChange(data: any): Promise<any> {
		return await Api.post('account/request-username-change', data);
	},

	async requestEmailChange(data: any): Promise<any> {
		return await Api.post('account/request-email-change', data);
	},
};
