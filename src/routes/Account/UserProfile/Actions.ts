import { DataStore } from 'core/data';
import { UserProfile } from 'core/auth';

const dataStore = DataStore<UserProfile>({
	endpoint: 'profile',
	store: 'user',
	key: 'userId',
	cache: true,
});

export default {
	...dataStore,
	async save (model: UserProfile) {
		return await dataStore.save(model);
	},
};
