import * as React from 'react';
import { autobind } from 'core-decorators';

import * as Panel from 'components/Panel';
import * as Form from 'components/Form';

import { UserProfile } from 'core/auth';
import schema from '../schema';

interface Props extends Form.Props<UserProfile> {}
interface State extends Form.State<UserProfile> {}

@autobind
class UserProfileForm extends Form.Master<UserProfile, Props, State> {
	constructor (props: Props, context) {
		super(props, context);

		this.domain = UserProfile.domain;
		this.key = UserProfile.key;
		this.schema = schema;
		this.id = `form-${UserProfile.domain}`;

		this.state = {
			model: props.model,
			fields: [],
		};
	}

	componentWillReceiveProps(nextProps: Props) {
		if (!nextProps.isSaving) {
			this.setState(this.getPartialState(nextProps));
		}
	}

	componentDidMount() {
		const state = this.getPartialState();
		this.setState(state, super.componentDidMount);
	}

	render(): JSX.Element {
		const { isSaving, onCancel } = this.props;
		const actions: Panel.Action[] = [
			{ id: 'cancel', icon: 'clear', disabled: isSaving, label: 'Cancel', onClick: onCancel },
			{ id: 'save', icon: 'save', disabled: isSaving, label: 'Save', onClick: this.handleSave },
		];

		return (
			<Panel.Element actions={actions}>
				{ super.render() }
			</Panel.Element>
		);
	}

	getPartialState(nextProps = null): State {
		const { model } = nextProps || this.props;
		const fields: Form.Field[] = [
			{ id: 'username', type: 'text', name: 'username', label: 'User Name', value: model.username},
			{ id: 'firstName', type: 'text', name: 'firstName', label: 'First Name', value: model.firstName},
			{ id: 'lastName', type: 'text', name: 'lastName', label: 'Last Name', value: model.lastName},
			{ id: 'email', type: 'text', name: 'email', label: 'Email', value: model.email},
			{ id: 'phoneNumber', type: 'text', name: 'phoneNumber', label: 'Phone Number', value: model.phoneNumber},
			{ id: 'avatar', type: 'text', name: 'avatar', label: 'Avatar', value: model.avatar},
			{ id: 'bio', type: 'textarea', name: 'bio', label: 'Biography', value: model.bio},
			{ id: 'iso2', type: 'text', name: 'iso2', label: 'Country', value: model.iso2},
		];

		return { model, fields };
	}
}

export default UserProfileForm;
