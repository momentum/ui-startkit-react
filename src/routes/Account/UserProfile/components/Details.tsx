import * as React from 'react';
import * as Panel from 'components/Panel';
import { autobind } from 'core-decorators';

import { UserProfile } from 'core/auth';
import { InfoPanel } from 'components/InfoPanel';

interface Props extends React.HTMLProps<HTMLDivElement> {
	model: UserProfile;
	isLoading?: boolean;
	onEdit: (item: UserProfile) => void;
}

@autobind
export default class UserProfileDetails extends React.Component<Props> {
	constructor (props: Props) {
		super(props);
	}

	render() : JSX.Element {
		const { model = new UserProfile(), onEdit} = this.props;

		return (
			<Panel.Element
				actions={[
					{ id: 'edit', icon: 'mode_edit', disabled: false, label: 'Edit', onClick: onEdit },
				]}>
				<InfoPanel
					header={{ id: 'user-profile-info', title: model.firstName }}
					body={{
						rows: [
							[
								{ id: 'username', label: 'User Name', value: model.username},
								{ id: 'firstName', label: 'First Name', value: model.firstName},
								{ id: 'lastName', label: 'Last Name', value: model.lastName},
								{ id: 'email', label: 'Email', value: model.email},
								{ id: 'phoneNumber', label: 'Phone Number', value: model.phoneNumber},
								{ id: 'avatar', label: 'Avatar', value: model.avatar},
								{ id: 'bio', label: 'Biography', value: model.bio},
								{ id: 'iso2', label: 'Country', value: model.iso2},
							],
						],
					}}
				/>
			</Panel.Element>
		);
	}
}
