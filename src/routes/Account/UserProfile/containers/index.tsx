import * as React from 'react';
import { autobind } from 'core-decorators';
import { withRouter } from 'react-router';
import { branch } from 'baobab-react/higher-order';
import { animateTransition } from 'core/decorators';

import * as Container from 'components/Container';
import * as Panel from 'components/Panel';
import * as Component  from '../components';

import { UserProfile } from 'core/auth';
import Actions from '../Actions';

interface Props extends Container.Props<UserProfile, Container.Params> {
	model: UserProfile;
}
interface State extends Container.State<UserProfile> {}

/**
 * Baobab cursors used by country domain
 */
@branch({
	isLoading: ['ajax', 'loading'],
	isSaving: ['ajax', 'saving'],
	selectedModel: ['user', 'model'],
})
@autobind
class UserProfilePage extends Container.Master<UserProfile, Props, State>  {
	constructor(props: Props) {
		super(props);

		this.domain = UserProfile.domain;
		this.key = UserProfile.key;

		this.state = {};
	}

	/**
	 * Render the UserProfile container
	 * @returns {JSX.Element}
	 * @memberof UserProfilePage
	 */
	render(): JSX.Element {
		const { isLoading, isSaving, selectedModel } = this.props;
		const { editingModel } = this.state;

		return (
			<Panel.Container>
				{!editingModel &&
				<Component.Details
					model={selectedModel}
					isLoading={isLoading}
					onEdit={() => this.handleAction(selectedModel, 'edit')}
				/>}

				{editingModel &&
				<Component.Form
					model={editingModel}
					isSaving={isSaving}
					onCancel={() => this.handleAction(selectedModel)}
					onSubmit={(data) => Actions.save(data)}
				/>}
			</Panel.Container>
		);
	}
}

const RoutedComponent = withRouter(UserProfilePage);

export default animateTransition()(RoutedComponent);
