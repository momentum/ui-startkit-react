import UserProfilePage from './containers';

const key = 'userprofile';
const path = '/user-profile';

export default {
	key,
	path,
	component: UserProfilePage,
	exact: true,
	routes: [
		{ path: `${path}/:id?/:action?`, component: UserProfilePage },
	],
	data: {
		icon: 'account_box',
		title: 'User Profile',
		description: 'User Profile',
	},
};
