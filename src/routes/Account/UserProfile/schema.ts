import definitions from 'routes/schemas.definitions';

export default {
	type: 'object',
	properties: {
		username: { $ref: '#/definitions/hasString' },
		firstName: { $ref: '#/definitions/hasString' },
		lastName: { $ref: '#/definitions/hasString' },
		email: { $ref: '#/definitions/hasString' },
		phoneNumber: { $ref: '#/definitions/hasString' },
		avatar: { $ref: '#/definitions/hasString' },
		bio: { $ref: '#/definitions/hasString' },
		iso2: { $ref: '#/definitions/hasString' },
	},
	required: [
		'firstName',
		'lastName',
		'email',
		'phoneNumber',
		'avatar',
	],
	definitions,
};
