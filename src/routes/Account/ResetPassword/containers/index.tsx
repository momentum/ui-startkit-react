import * as React from 'react';
import { autobind } from 'core-decorators';
import { withRouter } from 'react-router';
import { animateTransition } from 'core/decorators';
import { UserAuth } from 'core/auth';
import { ValidationError } from 'core/exception';

import * as Container from 'components/Container';
import ResetPasswordForm from '../components/Form';
import ResetPassword from '../models/ResetPassword';

import '../../index.scss';

interface Props extends Container.Props<ResetPassword, {}> {}
interface State extends Container.State<ResetPassword> {
	isSaving?: boolean;
}

@autobind
class ResetPasswordPage extends Container.Master<ResetPassword, Props, State>  {
	constructor(props: Props) {
		super(props);

		this.domain = ResetPassword.domain;
		this.key = ResetPassword.key;

		this.state = {
			editingModel: new ResetPassword(),
		};
	}

	async handleSubmit ({email}) {
		try {
			this.setState({ isSaving: true });
			const response = await UserAuth.resetPassword(email);
			const errors: ValidationError = response.error
				? new ValidationError(response.error.message)
				: undefined;

			this.setState({ errors, isSaving: false }, () => {
				if (!response.error && response.redirect) {
					this.props.history.replace(response.redirect);
				}
			});
		} catch (e) {
			this.setState({ isSaving: false });
		}
	}

	render(): JSX.Element {
		const { editingModel, errors, isSaving } = this.state;

		return (
			<ResetPasswordForm
				model={editingModel}
				isSaving={isSaving}
				errors={errors}
				onCancel={() => this.props.history.replace('')}
				onSubmit={(model) => this.handleSubmit(model)}
			/>
		);
	}
}

const RoutedComponent = withRouter(ResetPasswordPage);

export default animateTransition(150)(RoutedComponent);
