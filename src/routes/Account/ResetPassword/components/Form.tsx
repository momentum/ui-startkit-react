import * as React from 'react';
import { autobind } from 'core-decorators';
import { Link } from 'react-router-dom';

import * as Form from 'components/Form';

import ResetPassword from '../models/ResetPassword';
import schema from '../schema';

interface Props extends Form.Props<ResetPassword> {}
interface State extends Form.State<ResetPassword> {}

@autobind
class ResetPasswordForm extends Form.Master<ResetPassword, Props, State> {
	constructor (props: Props, context) {
		super(props, context);

		this.domain = ResetPassword.domain;
		this.key = ResetPassword.key;
		this.schema = schema;
		this.id = `form-${ResetPassword.domain}`;

		this.state = {
			fields: [],
			model: new ResetPassword(),
		};
	}

	componentDidMount() {
		const { model, isSaving } = this.props;

		this.setState({
			model,
			fields: [{ id: 'email', name: 'email', type: 'text', label: 'Email' }],
			actions: [{ id: 'submit', icon: 'save', disabled: isSaving, label: 'Submit', onClick: this.handleSave }],
		}, super.componentDidMount);
	}

	componentWillReceiveProps(nextProps, nextState) {
		if (nextProps.errors) {
			this.setState({ errors: nextProps.errors  }, super.componentDidMount);
		}
	}

	render(): JSX.Element {
		return (
			<div id="page-reset-password" className="page-account">
				{ super.render() }
				<div>
					<p>Already have an account? <Link to={'/login'}>Login to your account</Link>.</p>
				</div>
			</div>
		);
	}
}

export default ResetPasswordForm;
