export default class ResetPassword {
	email: string = '';

	static domain = 'resetpassword';
	static key = 'email';
}
