import definitions from 'routes/schemas.definitions';

export default {
	type: 'object',
	properties: {
		email: { $ref: '#/definitions/email' },
	},
	required: [ 'email' ],
	definitions,
};
