import ResetPasswordPage from './containers';

export default {
	key: 'reset-password',
	path: '/reset-password',
	component: ResetPasswordPage,
	exact: true,
	data: {
		icon: 'lock open',
		title: 'Reset Password',
		description: 'Reset Password',
	},
};
