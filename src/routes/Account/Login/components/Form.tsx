import * as React from 'react';
import { autobind } from 'core-decorators';
import { Link } from 'react-router-dom';

import * as Form from 'components/Form';

import Login from '../models/Login';
import schema from '../schema';

interface Props extends Form.Props<Login> {}
interface State extends Form.State<Login> {}

@autobind
class LoginForm extends Form.Master<Login, Props, State> {
	constructor (props: Props, context) {
		super(props, context);

		this.domain = Login.domain;
		this.key = Login.key;
		this.schema = schema;
		this.id = `form-${Login.domain}`;

		this.state = {
			fields: [],
			model: new Login(),
		};
	}

	componentDidMount() {
		const { model, isSaving, onCancel } = this.props;

		const fields: Form.Field[] = [
			{ id: 'username', name: 'username', type: 'text', label: 'Username' },
			{ id: 'password', name: 'password', type: 'password', label: 'Password' },
			{ id: 'rememberMe', name: 'rememberMe', type: 'checkbox', label: 'Remember Me' },
		];

		const actions = [
			{ id: 'return', icon: 'clear', disabled: isSaving, label: 'Return', onClick: onCancel },
			{ id: 'submit', icon: 'save', disabled: isSaving, label: 'Submit', onClick: this.handleSave },
		];

		this.setState({ model, fields, actions }, super.componentDidMount);
	}

	componentWillReceiveProps(nextProps, nextState) {
		this.setState({ errors: nextProps.errors }, super.componentDidMount);
	}

	render(): JSX.Element {
		return (
			<div id="page-login" className="page-account">
				{ super.render() }
				<div>
					<p>Don't have an account? <Link to={'/signup'}>Create one</Link>.</p>
					<p>Don't remember your password? <Link to={'/reset-password'}>Reset you password</Link>.</p>
				</div>
			</div>
		);
	}
}

export default LoginForm;
