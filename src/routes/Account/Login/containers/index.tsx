import * as React from 'react';
import { autobind } from 'core-decorators';
import { withRouter } from 'react-router';
import { animateTransition } from 'core/decorators';
import { UserAuth } from 'core/auth';
import { ValidationError } from 'core/exception';

import * as Container from 'components/Container';
import LoginForm from '../components/Form';

import Login from '../models/Login';

import '../../index.scss';

interface Props extends Container.Props<Login, {}> {}
interface State extends Container.State<Login> {
	isSaving?: boolean;
}

@autobind
class LoginPage extends Container.Master<Login, Props, State>  {
	constructor(props: Props) {
		super(props);

		this.domain = Login.domain;
		this.key = Login.key;

		this.state = {
			editingModel: new Login(),
		};
	}

	async handleSubmit (model) {
		const { username, password } = model;

		try {
			this.setState({ isSaving: true });
			const response = await UserAuth.login(username, password);
			const errors: ValidationError = response.error
				? response.error
				: undefined;

			this.setState({ errors, isSaving: false }, () => {
				if (!response.error && response.redirect) {
					this.props.history.replace(response.redirect);
				}
			});
		} catch (e) {
			this.setState({ isSaving: false });
		}
	}

	render(): JSX.Element {
		const { editingModel, errors, isSaving } = this.state;

		return (
			<LoginForm
				model={editingModel}
				isSaving={isSaving}
				errors={errors}
				onCancel={() => this.props.history.replace('')}
				onSubmit={(model) => this.handleSubmit(model)}
			/>
		);
	}
}

const RoutedComponent = withRouter(LoginPage);

export default animateTransition(150)(RoutedComponent);
