import LoginPage from './containers';

export default {
	key: 'login',
	path: '/login',
	component: LoginPage,
	exact: true,
	data: {
		icon: 'perm identity',
		title: 'Login',
		description: 'Login',
	},
};
