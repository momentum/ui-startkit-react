import definitions from 'routes/schemas.definitions';

export default {
	type: 'object',
	properties: {
		username: { $ref: '#/definitions/hasString' },
		password: { $ref: '#/definitions/hasString' },
	},
	required: [ 'username', 'password' ],
	definitions,
};
