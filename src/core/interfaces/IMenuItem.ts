export default interface IMenuItem {
	id: string;
	href: string | Function;
	label: string;
	icon?: string;
	children?: Array<IMenuItem>;
	order?: number;
}
