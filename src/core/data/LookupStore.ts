﻿import { DataStore } from 'core/data';

export default DataStore<any>({endpoint: 'lookup',  store: 'lookup', cache: true});
