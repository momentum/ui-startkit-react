// https://github.com/facebook/emitter
// import { EventEmitter } from 'fbemitter';
import State from 'state';
import { IKey, IDataStore } from './';
import Api from 'core/api';
import { isArray } from 'util';
// import { ValidationError } from 'core/exception';

interface DataStoreProps<T> {
	endpoint: string;
	store: string;
	key?: string;
	cache?: boolean;
	onRealtimeUpdate?: () => void;
	transformResponse?: (model: T[]) => void;
}
/**
* Constructor for actual data model.
*
* @param   {string}  [endpoint]  Name of the API endpoint
* @constructor
*/
export default function DataStore<T>({
		endpoint,
		store,
		key = 'id',
		cache = false,
		onRealtimeUpdate,
		transformResponse,
	}: DataStoreProps<T>): IDataStore<T> {

	const getCursor = (type = null) => State.select(!type ? store : [store].concat(type));

	// const emitter: any = new EventEmitter();
	const rootCursor: any = State.select();
	const errorCursor: any = State.select('error');
	const ajaxCursor: any = rootCursor.select('ajax');
	const domainCursor = getCursor();
	const modelsCursor = getCursor('models');
	const modelCursor = getCursor('model');

	const selectedModelCursor = getCursor('selectedModelId');
	const checkedCursor = getCursor('checkedModels');
	// const pendingUpdatesCursor = getCursor('pendingUpdates');

	// Initialize default values.
	let model: T;

	// Subscribe to specified endpoint
	if (!endpoint) {
		throw Error('Data Store domain needs to be set');
	}

	function clearCache(): Promise<void | {}> {
		return new Promise((resolve) => {
			rootCursor.once('update', () => resolve());

			// We now merge the serialized data instead of using the plain get/set.
			// The reason for this is to avoid corruption of monkeys, which do not work well when copied
			// from one tree to another. Serialize removes them from data that is being injected, while
			// merge ensures that original monkeys are left alive.

			// As a result of this change, clearCache will not clear brand new properties added to the
			// baobab branch. To make sure they are cleared, ensure that they have an empty default value.
			const defaultDomain = domainCursor.get();
			if (defaultDomain) {
				rootCursor.merge(defaultDomain);
			}
			State.commit();
		}).catch(setError);
	}

	function setEndpoint(url: string): void {
		endpoint = url;
	}

	async function find(identifier?: any, params?: any): Promise<T> {
		setProgress(true);

		const url = `${endpoint}` + (identifier ? `/${identifier}` : '');
		const response = await Api.get(url, params);
		if (response.error) {
			setError(response.error);
		}
		else {
			model = Array.isArray(response) ? response[0] : response;
			modelCursor.set(model);
			setProgress(false);
			return model;
		}
	}

	async function loadModels(params: any = {}, url?: string): Promise<Array<T>> {
		setProgress(true);
		const response = await Api.get(url || endpoint, params);

		if (response.error) {
			setError(response.error);
		}
		else {
			const models = typeof transformResponse !== 'function'
				? response
				: transformResponse(response);

			modelsCursor.set(models);
			setProgress(false);

			return models;
		}
	}

	async function get(params: any = {}, url?: string, isSingleModel: boolean = false): Promise<T[]> {
		const models = modelsCursor.get();
		if (cache && models.length) {
			return new Promise<T[]>((resolve) => resolve(models));
		}
		return await loadModels(params, url);
	}

	async function save(data: T, identifier?: IKey, url?: string): Promise<void | any> {
		const newUrl = url || endpoint;
		const finalUrl = !identifier
			? newUrl
			: `${newUrl}/${identifier}`;

		setProgress(true, 'saving');
		const fn = !identifier ? Api.post : Api.put;
		const response = await fn(finalUrl, data);
		if (response.error) {
			setError(response.error);
		}
		else {
			setProgress(false, 'saving');
			return response;
		}
	}

	async function updateStore(data: T | T[] | IKey, action: string) {
		const models = modelsCursor.get();
		let updatedModels;

		switch (action) {
			case 'add':
				modelsCursor.splice([1, 0, data]);
				break;
			case 'update':
				const index = models.findIndex((model) => model[key] === data[key]);
				updatedModels = [...models.slice(0, index), data, ...models.slice(index + 1)];
				modelsCursor.set(updatedModels);
				break;
			case 'delete':
				modelsCursor.unset(data);
				break;
			default:
				break;
		}

		State.commit();
	}

	async function remove(ids: IKey[], url?: string): Promise<void | any> {
		const qs = isArray(ids) && ids.length > 1
			? `?ids=${ids.join(',')}`
			: `/${ids}`;
		const response = await Api.remove(`${url || endpoint}${qs}`);

		setProgress(true, 'deleting');
		if (response.error) {
			setError(response.error);
		}
		else {
			this.clearCheck();
			setProgress(false, 'deleting');
			return response;
		}
	}

	async function toggleSelection (item: any): Promise<void | {}> {
		return new Promise((resolve) => {
			rootCursor.once('update', () => resolve());
			selectedModelCursor.set(
				typeof item === 'object'
					? (item[key] || item.cid)
					: item,
			);
			State.commit();
		}).catch(setError);
	}

	async function clearSelection (): Promise<void | {}> {
		return new Promise((resolve) => {
			rootCursor.once('update', () => resolve());
			selectedModelCursor.set(0);
			State.commit();
		}).catch(setError);
	}

	async function toggleCheck (itemId: IKey | IKey[]): Promise<void | {}> {
		return new Promise((resolve) => {
			rootCursor.once('update', () => resolve());

			// Accept an array as `itemId` to replace whatever the
			// selection is (used to implement check/uncheck-all).
			if ( Array.isArray(itemId) ) {
				checkedCursor.set(itemId);
			}
			// Otherwise proceed to check/uncehck single id.
			else {
				const items = checkedCursor.get();
				const index = items.indexOf(itemId);

				if (index === -1) {
					checkedCursor.push(itemId);
				}
				else {
					checkedCursor.unset(index);
				}
			}
			State.commit();
		}).catch(setError);
	}

	async function clearCheck (): Promise<void | {}> {
		return new Promise((resolve) => {
			rootCursor.once('update', () => resolve());
			checkedCursor.set([]); // domainCursor.get('checkedModels')
			State.commit();
		}).catch(setError);
	}

	async function upload(files: any, route: string, identifier?: IKey): Promise<void> {
		const url = `${endpoint}${identifier ? '/' + identifier : ''}/${route || 'upload'}`;
		return await Api.upload(url, files);
	}

	function setProgress (inProgress: boolean, action = 'loading'): void {
		ajaxCursor.set(action, inProgress);
		State.commit();
	}

	function setError (error: string | Error) {
		const err: Error = error instanceof Error
			? error
			: new Error(error);
		// When receiving an error, save it to the global state.
		errorCursor.set(err);
		setProgress(false);
		throw err;
	}

	function activateListeners(): void {
		if (typeof onRealtimeUpdate === 'function') {
			onRealtimeUpdate();
		}
	}

	return {
		activateListeners,
		clearCache,
		clearCheck ,
		clearSelection,
		find,
		get,
		remove,
		save,
		setEndpoint,
		toggleCheck,
		toggleSelection,
		upload,
		updateStore,
	};
}

