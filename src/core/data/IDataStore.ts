import { IKey } from 'core/data';

export default interface IDataStore<T> {
	clearCache(): Promise<void | {}>;
	setEndpoint(apiUrl: string): void;
	find(identifier?: any, params?: any): Promise<T>;
	get(params?: any, apiUrl?: string, isSingleModel?: boolean): Promise<T[]>;
	save(data: T, apiUrl?: string, identifier?: IKey): Promise<void | any>;
	remove(itemIds: IKey[], apiUrl?: string): Promise<void | any>;
	toggleSelection (item: T): Promise<void | {}>;
	clearSelection (): Promise<void | {}>;
	toggleCheck (itemId: IKey): Promise<void | {}>;
	clearCheck (): Promise<void | {}>;
	toggleCheck (itemId: IKey | IKey[]): Promise<void | {}>;
	upload(files: any, route: string, identifier?: IKey): Promise<void>;
	activateListeners(): void;
	updateStore(models: T | T[], action: string): void;
}
