interface DataStoreProps {
	endpoint: string;
	branch: string;
	key: string;
	cache: boolean;
}
