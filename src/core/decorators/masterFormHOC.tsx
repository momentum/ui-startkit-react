import * as React from 'react';

// State of the HOC you need to compute the InjectedProps
// Props you want the resulting component to take (besides the props of the wrapped component)
interface ExternalProps {}

// Props the HOC adds to the wrapped component
export interface InjectedProps {
	onClick<P>(event: React.MouseEvent<P>);
	onFieldChange(id: string, value: any);
}

// Props the HOC adds to the wrapped component
export interface InjectedState {
	errors?: any;
	model?: any;
}

// Options for the HOC factory that are not dependent on props values
interface Options {
	key?: string;
	schema?: any;
}

const masterFormFactory = ({ key = 'Default value' }: Options = {}) => <TOriginalProps extends {}, TOriginalState extends {}>(
	Component:
	| React.ComponentClass<TOriginalProps & InjectedProps>
	| React.StatelessComponent<TOriginalProps & InjectedProps>,
) => {
	// Do something with the options here or some side effects

	type ResultProps = TOriginalProps & ExternalProps;
	type ResultState = TOriginalState & InjectedState;

	return class MasterFormComponent<P, S> extends React.Component<ResultProps & P, ResultState & S> {
		// Define how your HOC is shown in ReactDevTools
		static displayName = `BaseForm(${Component.displayName || Component.name})`;

		constructor(props: ResultProps & P) {
			super(props);

			this.handleClick = this.handleClick.bind(this);
			console.log(this.state);
		}

		// Implement other methods here
		render(): JSX.Element {
			// Render all your added markup

			return (
				<Component onClick={this.handleClick} onFieldChange={this.handleModelValueChange} {...this.props} {...this.state} />
			);
		}

		private handleClick<P>(event: React.MouseEvent<P>) {
			event.preventDefault();
			console.log(event.target);
		}

		private handleModelValueChange(id: string, value: any): void {
			const state: InjectedState = { model: Object.assign({}, this.state.model, {[id]: value} ) };
			this.setState(state);
		}
	};
};

export default masterFormFactory;
