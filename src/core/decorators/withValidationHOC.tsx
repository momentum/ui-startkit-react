// https://code.tutsplus.com/tutorials/validating-data-with-json-schema-part-1--cms-25343

import * as React from 'react';
import Ajv from 'ajv';

interface MainProps {
	schema: any;
	children?: any;
}

interface Param {
	missingProperty: any;
}

interface Err extends Ajv.ErrorObject {
	params: Param;
}

interface MainState {
	validator: any;
	errors?: any;
}

const ajv: Ajv.Ajv = Ajv({ allErrors: true, validateSchema: true });

export default function withValidation(schema: any) {
	return function component<Props extends MainProps, State extends MainState, ComponentState>(
		WrappedComponent: { new(...args : any[]): React.Component<Props, ComponentState> },
	) {

		function errorToProperty(err: Err) {
			const { params = { missingProperty: null } } = err;

			return (params.missingProperty)
				? params.missingProperty
				: undefined;
		}

		class ComponentHOC extends React.Component<Props & MainProps, State | MainState> {

			schema: any;
			validator: Ajv.ValidateFunction;
			errors: any;
			state: MainState = {
				validator: {},
			};

			constructor(props: Props, context) {
				super(props, context);
				this.validator = ajv.compile(props.schema);
			}

			componentDidMount() {
				/* code goes here */
			}

			async validate(value) {
				const valid = this.validator(value);
				if (valid) {
					return true;
				}

				const errors = this.validator.errors.reduce((result, err: Err) => {
					const prop = errorToProperty(err);
					const path = err.dataPath ? err.dataPath.substr(1) : null;

					const fullPath = path && prop ? `${path}.${prop}` : path || prop;

					result[fullPath] = {...err, path: fullPath };

					return result;
				}, {});

				this.setState({errors});

				return false;
			}

			render(): JSX.Element {
				return <WrappedComponent {...this.props} />;
			}
		}

		return ComponentHOC;
	};
}
