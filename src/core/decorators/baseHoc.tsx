import * as React from 'react';

// State of the HOC you need to compute the InjectedProps
interface State {}

// Props you want the resulting component to take (besides the props of the wrapped component)
interface ExternalProps {}

// Props the HOC adds to the wrapped component
export interface InjectedProps {
	onClick<P>(event: React.MouseEvent<P>);
}

// Options for the HOC factory that are not dependent on props values
interface Options {
	key?: string;
}

export const baseHocFactoryName = ({ key = 'Default value' }: Options = {}) => <TOriginalProps extends {}>(
	Component:
	| React.ComponentClass<TOriginalProps & InjectedProps>
	| React.StatelessComponent<TOriginalProps & InjectedProps>,
) => {
	// Do something with the options here or some side effects

	type ResultProps = TOriginalProps & ExternalProps;

	return class YourComponentName extends React.Component<ResultProps, State> {
		// Define how your HOC is shown in ReactDevTools
		static displayName = `YourComponentName(${Component.displayName || Component.name})`;

		constructor(props: ResultProps) {
			super(props);

			this.handleClick = this.handleClick.bind(this);

			this.state = {
				// Init the state here
			};
		}

		// Implement other methods here
		render(): JSX.Element {
			// Render all your added markup

			return (
				<Component onClick={this.handleClick} {...this.props} {...this.state} />
			);
		}

		private handleClick<P>(event: React.MouseEvent<P>) {
			console.log(event);
			event.preventDefault();
		}
	};
};
