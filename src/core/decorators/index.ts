// https://mikebridge.github.io/articles/getting-started-typescript-react-4/
// https://dev.to/danhomola/react-higher-order-components-in-typescript-made-simple
// https://stackoverflow.com/questions/44926061/using-hocs-on-generic-typescript-react-components

export { default as animateTransition } from './AnimateTransition';
