export default interface IApiSettings {
	url: string;
	prefix: string;
	contentType: string;
	clientId: string;
	clientSecret?: string;
	grantType:  string;
	loginUrl: string;
	resetPasswordUrl: string;
	signupUrl: string;
	profileUrl: string;
	unlinkUrl: string;
	unlinkMethod: string;
}
