import IApi, { EndpointParam } from 'core/api/IApi';
import nanoajax from 'nanoajax';  // https://github.com/yanatan16/nanoajax

import settings from 'settings';
import { UserAuth } from 'core/auth';
import { ValidationError } from 'core/exception';

function Api(): IApi {
	return {
		get,
		post,
		patch,
		put,
		remove,
		upload,
	};
	/**
	* Action method to get data from certain end point. This will always return a collection
	* of data.
	*
	* @param   {string}	endPoint	Name of the end point
	* @param   {{}}		parameters  Used query parameters
	*
	* @returns {Promise|*}
	*/
	function get(route: string, data?: any, params?: EndpointParam): Promise<any> {
		return request(route, 'GET', data, params);
	}

	/**
	* Action method to create new object to specified end point.
	*
	* @param   {string}	endPoint	Name of the end point
	* @param   {{}}		data		Data to update
	*
	* @returns {Promise|*}
	*/
	function post(route: string, data?: any, params?: EndpointParam): Promise<any> {
		return request(route, 'POST', data, params);
	}

	function patch(route: string, data?: any, params?: EndpointParam): Promise<any> {
		return request(route, 'PATCH', data, params);
	}

	function upload(route: string, data?: any, params?: EndpointParam): Promise<any> {
		return request(route, 'UPLOAD', data, params);
	}

	/**
	* Action method to update specified end point object.
	*
	* @param   {string}	endPoint	Name of the end point
	* @param   {number}	identifier  Identifier of endpoint object
	* @param   {{}}		data		Data to update
	*
	* @returns {Promise|*}
	*/
	function put(route: string, data?: any, params?: EndpointParam): Promise<any> {
		return request(route, 'PUT', data, params);
	}

	/**
	* Action method to delete specified object.
	*
	* @param   {string}	endPoint	Name of the end point
	* @param   {number}	identifier  Identifier of endpoint object
	*
	* @returns {Promise|*}
	*/
	function remove(route: string, data?: any, params?: EndpointParam): Promise<any> {
		return request(route, 'DELETE', data, params);
	}

	function request(route: string, method: string, data?: any, params: EndpointParam = {}): Promise<any> {
		if (!route) {
			throw new ValidationError('API endpoint is missing');
		}
		const contentType = !(typeof (data) === 'object' && data.constructor.name === 'FormData')
			? params.contentType || settings.api.contentType
			: null;
		let url = buildUrl(route, data);

		let configRequest = {
			body: null,
			method : method,
			headers: { 'Content-Type': contentType },
		};

		switch (method) {
			case 'GET':
			case 'DELETE':
				const params = getParams(data);
				if (params) {
					url += `?${params}`;
				}
				break;
			case 'PUT':
			case 'PATCH':
			case 'POST':
				if (data.constructor.name === 'FormData') {
					configRequest.body = new FormData(data);
				} else {
					configRequest.body = typeof(data) === 'object' ? JSON.stringify(data) : data;
				}
				break;
		}

		if (!params.noCredentials) {
			if (settings.authHeader && settings.authToken) {
				let token = UserAuth.getToken();
				let authHeader = `${settings.authToken} ${token}`;
				configRequest.headers[settings.authHeader] = authHeader;
			}
		}

		const request = {...configRequest, url };

		return new Promise((resolve, reject) => {
			return nanoajax.ajax(request, (code, response, xmlHttpRequest) => {
				if (code === 204) {
					resolve();
				}
				if (code === 0) {
					reject(new ValidationError('Error on communicating to the server. Please, check if the device is connected to the internet or contact the server administrator.'));
				}
				try {
					const apiResponse = JSON.parse(response);
					const { message, error_description } = apiResponse;
					switch (code) {
						case 401:
							UserAuth.logout(error_description || message)
								.then(({redirect}) => window.location.href = redirect);
							break;
					}
					resolve(apiResponse);
				} catch (e) {
					reject(new ValidationError(`Request response is not a valid JSON ${url}: ${request.method}: ${e.message}`));
				}
			});
		});
	}

	function getParams(data: any = {}, parameters: string[] = []): string {
		return Object.keys(data).reduce((queryString, prop) => {
			if (!parameters.length || (parameters.length > 0 && parameters[prop])) {
				queryString.push(`${encodeURIComponent(prop)}=${encodeURIComponent(data[prop])}`);
			}
			return queryString;
		}, []).join('&');
	}

	function buildUrl(resource: string, parameters: any = {}): string {
		const endpoint = Object.keys(parameters).reduce((result, prop) => {
			const key = `:${prop}`;
			if (result.indexOf(key) > -1 && parameters[prop]) {
				result = result.replace(key, parameters[prop]);
				delete parameters[prop];
			}
			return result;
		}, resource);

		const apiEndPoint = endpoint.substr(0, 1) === '/'
			? endpoint
			: `/${settings.api.prefix}/${endpoint}`;

		return `${settings.api.url}${apiEndPoint}`;
	}
}

export default Api();
