
export type EndpointParam = {
	contentType?: string;
	noCredentials?: boolean;
};

export default interface IApi {
	get: (route: string, data?: any, params?: EndpointParam) => Promise<any>;
	post: (route: string, data?: any, params?: EndpointParam) => Promise<any>;
	put: (route: string, data?: any, params?: EndpointParam) => Promise<any>;
	patch: (route: string, data?: any, params?: EndpointParam) => Promise<any>;
	remove: (route: string, data?: any, params?: EndpointParam) => Promise<any>;
	upload: (route: string, data?: any, params?: EndpointParam) => Promise<any>;
}
