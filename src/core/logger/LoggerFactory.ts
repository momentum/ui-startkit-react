import * as React from 'react';
import * as ReactDOM from 'react-dom';
import NotificationSystem from 'react-notification-system';
import LoggerParams from './LoggerParams';
import ILoggerFactory from './ILoggerFactory';

let notifier = null;

const idify = (notification: LoggerParams, customId: string) => {
	// Get notification DOM element and stick an id to it.
	const refs = { refs: {} };
	const containerElement = notifier.refs[`container-${notification.position}`] || refs;
	const notificationElement = containerElement.refs[`notification-${notification.uid}`] || refs;
	const notificationNode = ReactDOM.findDOMNode(notificationElement);

	if (notificationNode) {
		const loggerId = ['logger', notification.level, customId || notification.uid];
		notificationNode.setAttribute('id', loggerId.join('-'));
	}
};

function buildLogger (level: string, params: LoggerParams | string): void {
	const options = typeof params === 'string'
		? { message: params }
		: params;

	if (!options.message) {
		return;
	}

	const obj = Object.assign({}, new LoggerParams(level), options);

	if (!notifier) {
		const element = document.createElement('div');
		const wrapper = document.body.appendChild(element);
		element.setAttribute('id', 'notifier');
		notifier = ReactDOM.render(React.createElement(NotificationSystem), wrapper);
	}

	setTimeout(() => {
		const notification = notifier.addNotification(obj);
		idify(notification, obj.id);
	});
}

const loggerFactory: ILoggerFactory = {
	info (params: LoggerParams | string): void { buildLogger('info', params); },
	success (params: LoggerParams | string): void { buildLogger('success', params); },
	warning (params: LoggerParams | string): void { buildLogger('warning', params); },
	error (params: LoggerParams | string): void { buildLogger('error', params); },
};

export default loggerFactory;
