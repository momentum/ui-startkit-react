export default {
	topLeft: 'tl',
	topRight: 'tr',
	topCenter: 'tc',
	bottomLeft: 'bl',
	bottomRight: 'br',
	bottomCenter: 'bc',
};
