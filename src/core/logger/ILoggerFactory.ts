import LoggerParams from './LoggerParams';

export default interface ILoggerFactory {
	info: (params: LoggerParams | string) => void;
	success: (params: LoggerParams | string) => void;
	warning: (params: LoggerParams | string) => void;
	error: (params: LoggerParams | string) => void;
}
