import LoggerPosition from './LoggerPosition';

// Levels
const LEVELS = {
	success: 'Success',
	error: 'Error',
	warning: 'Warning',
	info: 'Information',
};

class LoggerParams {
	id?: string;
	uid?: string;
	title: string;
	message: string;
	level?: string;
	position?: string = LoggerPosition.topRight;
	autoDismiss?: number = 3;
	dismissible?: boolean = true;
	action?: string = '';

	constructor(level: string = LEVELS.info) {
		this.level = level;
		this.title = LEVELS[level];
	}
}

export default LoggerParams;
