export default interface SignalREvent {
	userId: string;
	module: string;
	action: string;
	data: any;
}
