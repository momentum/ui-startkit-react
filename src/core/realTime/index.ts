import settings from 'settings';
import SignalRService from './SignalRService';
import { UserAuth } from 'core/auth';

export default class RealTime extends SignalRService {
	private static _instance: RealTime;

	public static getInstance(): RealTime {
		return RealTime._instance || (RealTime._instance = new RealTime());
	}

	constructor() {
		super(settings.realTime.host, 'MessagingHub', UserAuth.getToken(), settings.realTime.logging);
	}
}
