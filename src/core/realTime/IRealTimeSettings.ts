export default interface IRealTimeSettings {
	host: string;
	logging: boolean;
	id?: string;
	messageId?: string;
}
