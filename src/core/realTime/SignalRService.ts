import $ from 'jquery';
import 'ms-signalr-client';
import IRealTimeService from './IRealTimeService';
import SignalREvent from './SignalREvent';

export default class SignalRService implements IRealTimeService {
	token: string;
	handlers: any = {};
	connection: any;
	proxy: any;
	options: any;
	hub: string;
	stateConversion: any = {
		0: 'Connecting',
		1: 'Connected',
		2: 'Reconnecting',
		4: 'Disconnected',
	};

	constructor(host: string, hubName: string, authToken, logging: boolean = true) {
		if (!$.hubConnection) { return; }
		console.log('Connecting to realtime listeners ...');
		this.connection = $.hubConnection(host);
		this.proxy = this.connection.createHubProxy(hubName);
		this.hub = hubName;
		// $.signalR.ajaxDefaults.headers = { Authorization: 'Bearer ' + token };
		this.connection.qs = { Bearer: authToken };
		this.connection.logging = !!logging;

		this.connection.disconnected(() => {
			if (!!authToken) {
				setTimeout(() => { this.subscribe.bind(this); }, 1000);
			}
		});
		this.proxy.on('handleEvent', this.handleEvent.bind(this));
	}

	private connectionStateChanged({oldState, newState}: any) {
		console.info(`${this.hub} State change ${this.stateConversion[oldState]} => ${this.stateConversion[newState]}`);
	}

	listenStatesChanges({id, state}: any) {
		if (id) {
			console.info(`${this.hub} State: ${this.stateConversion[state]} - ${id}`);
			this.connection.stateChanged(this.connectionStateChanged.bind(this));
		}
	}

	subscribe(options: any = {}) {
		options.transport = options.transport; // || 'longPolling';
		options.withCredentials = false;
		this.connection.start(options).
			done(this.listenStatesChanges.bind(this)).
			fail(({message}) => { console.error({message, title: 'SignalR Incompatible Version'}); });
	}

	unSubscribe() {
		this.connection.stop();
		this.proxy = null;
	}

	handleEvent(event: SignalREvent) {
		console.info(event);
		let eventHandlers = (this.handlers[event.module] || {})[event.action] || [];
		eventHandlers.forEach((fn: any) => fn(event.data, event.userId));
	}

	on(module: string, action: string, fn: Function) {
		if (action === undefined) { action = '$undefined'; }
		this.handlers[module] = this.handlers[module] || {};
		this.handlers[module][action] = this.handlers[module][action] || [];
		this.handlers[module][action].push(fn);
	}

	off(module: string, action: string, fn: Function) {
		let eventHandlers = (this.handlers[module] && this.handlers[module][action]) || [];
		if (fn) {
			eventHandlers.splice(eventHandlers.indexOf(fn), 1);
		} else {
			if (eventHandlers[module]) {
				eventHandlers[module] = [];
			}
		}
	}

	invoke () {
		let len = arguments.length;
		let args = Array.prototype.slice.call(arguments);
		let callback;

		if (len > 1) {
			callback = args.pop();
		}

		this.proxy.invoke.apply(this.proxy, args).done(result => {
			if (callback) {
				callback(result);
			}
		});
	}
}
