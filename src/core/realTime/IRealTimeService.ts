import SignalREvent from './SignalREvent';

export default interface IRealTimeService {
	token: string;
	handlers: any;
	connection: any;
	proxy: any;
	options: any;
	hub: string;
	subscribe: (options?: any) => void;
	unSubscribe: () => void;
	handleEvent: (e: SignalREvent) => void;
	on(module: string, action: string, fn: Function): void;
	off(module: string, action: string, fn: Function): void;
	invoke (): void;
}
