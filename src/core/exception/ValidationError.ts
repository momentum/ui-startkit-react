import ExtendableError from 'es6-error';

export default class ValidationError extends ExtendableError {
	errors: string | string[];

	constructor(message: string, type?: string, field?: string) {
		super();
		this.errors = Array.isArray(message) ? message : [message];
		this.message = message;
		this.name = 'ValidationError';
	}
}
