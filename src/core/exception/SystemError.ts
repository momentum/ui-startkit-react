import ExtendableError from 'es6-error';

export default class SystemError extends ExtendableError {
	constructor(message?: string) {
		super('SystemError');
		this.name = 'SystemError';
		this.message = message;
	}
}
