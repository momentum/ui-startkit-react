﻿// import settings from 'settings';
// import LocalStorageTypes from './LocalStorageTypes';
// import CookieStorage from './CookieStorage';

// (function init() {
// 	switch (settings.localStorageMode) {
// 		case LocalStorageTypes.Local:
// 			storage = window.localStorage;
// 			if (!('localStorage' in window && window.localStorage !== null)) {
// 				throw new Error('Warning: Local Storage is disabled or unavailable');
// 			}
// 			break;
// 		case LocalStorageTypes.Session:
// 			storage = window.sessionStorage;
// 			if (!('sessionStorage' in window && window.sessionStorage !== null)) {
// 				throw new Error('Warning: Local Storage is disabled or unavailable');
// 			}
// 			break;
// 		case LocalStorageTypes.Cookie:
// 			storage = CookieStorage;
// 		break;
// 	}
// }());

export default {
	get(key: string): string {
		return window.localStorage.getItem(key);
	},

	set(key: string, value: any, time?: any): void {
		window.localStorage.setItem(key, value);
	},

	remove(key: string): void {
		window.localStorage.removeItem(key);
	},
};
