export default interface IStorage {
	get(key: string): any;
	set(key: string, value: any, time?: any): void;
	remove(key: string): void;
}
