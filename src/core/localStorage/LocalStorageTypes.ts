enum LocalStorageTypes {
	Local = 1,
	Session = 2,
	Cookie = 3,
}

export default LocalStorageTypes;
