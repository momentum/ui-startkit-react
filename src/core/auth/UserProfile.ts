import AccessLevel from './AccessLevel';

export class UserPreferences {
	showMyLocation: true;
	shareProfileAsPublic: true;
	allowNotifications: true;
	shareWhatsAppId: true;
	shareSkypeId: true;
	shareTwitterId: true;
	shareFacebookId: true;
	dateFormat: 'ddd; MMM d; yyyy';
	timezoneId: 56;
	utcOffset: 12.0;
	coverPhoto: 'maple-leaves-background.jpg';
	avatar: 'hair-black-eyes-blue-green-skin-tanned';
	favoriteCityId: 3801;
	targetCountry: 'CA';
}

export default class UserProfile {
	userId: number;
	username: string;
	firstName: string;
	lastName: string;
	email: string;
	phoneNumber: string;
	avatar: string;
	accessLevel: AccessLevel = new AccessLevel();
	twitterId?: string;
	skypeId?: string;
	whatsAppId?: string;
	facebookId?: string;
	bio?: string;
	iso2?: string;
	preferences?: UserPreferences = new UserPreferences();

	static domain = 'user-profile';
	static key = 'userId';

	constructor(props?: any) {
		if (props) {
			Object.assign(this, props);
		}
	}
}
