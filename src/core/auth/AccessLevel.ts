export default class AccessLevel {
	title: string;
	bitMask: number;
}
