﻿enum AuthenticationTypes {
	Local = 1,
	OpenId = 2,
	OAuth = 3,
	OAuth2 = 4,
}

export default AuthenticationTypes;
