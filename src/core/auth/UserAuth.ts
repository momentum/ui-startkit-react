import settings from 'settings';
import LocalStorage from 'core/localStorage';
import Api from 'core/api';
import ActionResult from './ActionResult';
import { userRoles, accessLevels }from './Access';
import { ValidationError } from 'core/exception';

const TOKEN_KEY = `${settings.tokenPrefix}_${settings.tokenName}`;
const USER_TOKEN_KEY = `${settings.tokenPrefix}_user`;

/**
 * Authenticate a user. Save a token string in Local Storage
 *
 * @param {string} token
 */
async function login(identifier: string, password: string): Promise<ActionResult> {
	const grantContent = `grant_type=${settings.api.grantType}`;
	const userNameContent = `${'&username='}${identifier}`;
	const passwordContent = `${'&password='}${password}`;
	const clientId = `${'&client_id='}${settings.api.clientId}`;

	const bodyContent = `${grantContent}${userNameContent}${passwordContent}${clientId}`;
	const apiResponse: ActionResult = new ActionResult();
	apiResponse.action = 'login';

	const result: any = await Api.post(`${settings.api.loginUrl}`,
		bodyContent,
		{ contentType: 'application/x-www-form-urlencoded', noCredentials: true },
	);

	if (result.error) {
		apiResponse.error = new ValidationError(result.error_description);
		clearTokens();
	} else {
		setToken(result.access_token);
		const user =  {username: result.username, role: result.role};
		setUser(user);
		apiResponse.redirect = settings.defaultRoute;
		apiResponse.data = user;
		apiResponse.message = 'User has been successfully authenticated';
	}
	return apiResponse;
}

async function resetPassword(email: string): Promise<ActionResult> {
	const apiResponse: ActionResult = new ActionResult();

	const result = await Api.patch(settings.api.resetPasswordUrl, { data: email, noCredentials: true});
	if ( !result.error) {
		apiResponse.redirect = settings.loginRedirect;
		apiResponse.message = 'User has been successfully authenticated';
	} else {
		apiResponse.error = new ValidationError(result.error_description);
	}
	return apiResponse;
}

/**
* Get a token value.
*
* @returns {string}
*/
function getToken() {
	return LocalStorage.get(TOKEN_KEY);
}

/**
 * Check if a user is authenticated - check if a token is saved in Local Storage
 *
 * @returns {boolean}
 */
function isAuthenticated() {
	const userToken = getToken();
	return userToken !== null && userToken !== undefined;
}

function getUser() {
	const user = LocalStorage.get(USER_TOKEN_KEY);
	return user ? JSON.parse(user) : {};
}

function setUser(user: any): void {
	LocalStorage.set(USER_TOKEN_KEY, JSON.stringify(user));
}

function setToken(token) {
	return LocalStorage.set(TOKEN_KEY, token);
}

function getPublicUser() {
	return {username: '', role: userRoles.public};
}

function isAuthorized(accessLevel, role) {
	role = (role || getUser().accessLevel) || getPublicUser().role;
	return accessLevel.bitMask <= role.bitMask;
}

function getAccessLevel() {
	return getUser().accessLevel || accessLevels.public;
}

function checkAuth(nextState, replace, accessLevel) {

	const isAuth = isAuthenticated();

	if ((accessLevel > 0 &&  !isAuth) || (accessLevel === 0 && isAuth)) {
		replace({
			pathname: '/',
			state: {nextPathname: nextState.location.pathname },
		});
	}

	if (isAuth && nextState.location.pathname === '/') {
		replace({
			pathname: settings.defaultRoute,
			state: {nextPathname: nextState.location.pathname },
		});
	}
}

function logout(message?: any): Promise<ActionResult> {
	return new Promise((resolve, reject) => {
		clearTokens();
		const apiResponse = new ActionResult();
		apiResponse.action = 'logout';
		apiResponse.message = message || 'User successfully logged out';
		apiResponse.redirect = settings.loginRedirect;
		resolve(apiResponse);
	});
}

function clearTokens(): void {
	LocalStorage.remove(TOKEN_KEY);
	LocalStorage.remove(USER_TOKEN_KEY);
}

export default {
	login,
	resetPassword,
	getToken,
	isAuthenticated,
	getUser,
	isAuthorized,
	getAccessLevel,
	checkAuth,
	logout,
};
