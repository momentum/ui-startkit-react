import { ValidationError } from 'core/exception';

export default class ActionResult {
	action: string;
	redirect: string;
	message: string;
	error: ValidationError;
	data: any;

	constructor(redirect?: string, message?: string) {
		this.redirect = redirect;
		this.message = message;
	}
}
