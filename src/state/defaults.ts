import { monkey } from 'baobab';

const defaultFilters = {
	pageSize: 100,
	pageNumber: 0,
};

function defaultDataset (extras = { key: 'id'}) {
	return Object.assign({
		models: [],
		// modelsMap: {},
		localFilters: Object.assign({}, defaultFilters),
		filters: monkey({
			cursors: {
				globalFilters: ['globalFilters'],
				localFilters: ['.', 'localFilters'],
			},
			get ({ globalFilters, localFilters }) {
				return Object.assign({}, globalFilters, localFilters);
			},
		}),
		checkedModels: [],
		selectedModelId: 0,
		selectedModel: monkey({
			cursors: {
				models: ['.', 'models'],
				selectedModelId: ['.', 'selectedModelId'],
			},
			get ({ selectedModelId, models = [] }) {
				return models.find((model) => model[extras.key] === selectedModelId) || models[0] || {};
			},
		}),
	}, extras);
}

// Default state values
const coreState = {
	// General App State
	appVersion: null,
	debugMode: process.env.NODE_ENV !== 'production',
	ajax: {
		loading: false,
		saving: false,
	},
	user: {
		model: {},
	},
	lookup: {
		models: {
			countries: [],
		},
	},
	country: defaultDataset({key: 'iso2'}),
	province: defaultDataset({key: 'code'}),
	notebook: defaultDataset(),
	pendingUpdates: {},
};

export default Object.assign({}, coreState);
