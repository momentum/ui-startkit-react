import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router } from 'react-router';
import createHistory from 'history/createBrowserHistory';
import { AppContainer } from 'react-hot-loader';
import configRoutes from './routes';

import './assets/styles/index.scss';

const ROOT_ELEMENT = document.getElementById('app');

function render (routes) {
	// Let's bind the component to the tree through the `root` higher-order component
	document.addEventListener('DOMContentLoaded', () => {
		ReactDOM.render(
			<AppContainer>
				<Router history={createHistory()}>
					{routes}
				</Router>
			</AppContainer>,
			ROOT_ELEMENT,
		);
	});
}

// Enable HMR and catch runtime errors in RedBox
// This code is excluded from production bundle
const hotModule = module as NodeModuleHot;
if (hotModule.hot) {
	hotModule.hot.accept('./routes', () => {
		const updatedRoutes = require('./routes').default;
		try {
			render(updatedRoutes());
		} catch (error) {
			const RedBox = require('redbox-react');
			ReactDOM.render(<RedBox error={error}/>, ROOT_ELEMENT);
		}
	});
}

render(configRoutes());
