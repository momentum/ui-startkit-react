import * as React from 'react';
import { withRouter, RouteComponentProps } from 'react-router';
import { autobind } from 'core-decorators';
import * as ReactGA from 'react-ga';
import Progress from 'react-progress-2';
import State from 'state';
import { Icon } from 'components';
import { root, branch } from 'baobab-react/higher-order';
import ErrorTracker from 'components/ErrorTracker';
import Logger from 'core/logger';

import { UserAuth, UserAction, UserProfile } from 'core/auth';
import LookupStore from 'core/data/LookupStore';
import PageInfo from '../PageInfo';
import LayoutSettings from '../LayoutSettings';
import settings from 'settings';

import Header from 'components/Header';
import Sidebar from 'components/Sidebar';
import Footer from 'components/Footer';
import SlidingPanel from 'components/SlidingPanel';
import Toggler from 'components/Toggler';

import RealTime from 'core/realTime';

// import * as menu from 'components/menu.json';
// routes: Array<IMenuItem> = menu['items'] as Array<IMenuItem>;
import './index.scss';

interface Props extends RouteComponentProps<{}> {
	dispatch: any;
	routeData: any;
	navigation: any;
	children: string | JSX.Element;
	user: UserProfile;
	loading?: boolean;
	debugMode: boolean;
	error: Error;
}

interface State {
	isSidebarCollapsed: boolean;
	page: PageInfo;
	layout: LayoutSettings;
}

const layoutProp = 'layout';

@branch({
	loading: ['ajax', 'loading'],
	user: ['user', 'model'],
	debugMode: ['debugMode'],
	error: ['error'],
})
@autobind
class WorkspaceLayout extends React.Component<Props, State> {
	currentRoute: string;
	realTime: RealTime;

	state = {
		isSidebarCollapsed: false,
		page: new PageInfo(),
		layout: LayoutSettings.getInstance(),
	};

	constructor(props: Props) {
		super(props);
	}

	componentWillReceiveProps (newProps) {
		this.preparePageInfo(newProps);
		Progress[newProps.loading ? 'show' : 'hideAll']();
	}

	async componentDidMount() {
		const isAuthenticated = UserAuth.isAuthenticated();

		this.preparePageInfo(this.props);

		// If user token does NOT exist, redirect user to login route
		if (isAuthenticated) {
			try {
				await Promise.all([
					UserAction.loadProfile(),
					LookupStore.get(),
				]);
				this.handleSuccessAuth();
				this.realTime = RealTime.getInstance();
				this.realTime.subscribe();

				this.realTime.on('Account', 'Connect', Logger.info);
				this.realTime.on('Account', 'Disconnect', Logger.info);
			} catch (e) {
				console.error(e.message);
				await UserAuth.logout(e.message);
			}
		} else {
			await UserAuth.logout();
		}
	}

	handleSuccessAuth () {
		if (settings.analyticsId) {
			// Initialize Google Analytics
			ReactGA.initialize(settings.analyticsId, { debug: false });
			// Log page view as soon as page is loaded
			this.logPageView();
			this.props.history.listen((nextLocation: any) => this.logPageView());
		}
	}

	logPageView() {
		const { pathname } = this.props.location;
		ReactGA.set({ page: pathname });
		ReactGA.pageview(pathname);
	}

	async handleLogout() {
		const { redirect } = await UserAuth.logout();
		this.realTime.unSubscribe();

		if (redirect) {
			this.props.history.push(redirect);
		}
	}

	handleTogglePanel(side: string, isCollapsed: boolean = false) {
		const { isSidebarCollapsed } = this.state;
		this.setState({ isSidebarCollapsed: !isSidebarCollapsed });
	}

	preparePageInfo ({ location, routeData }: Props) {
		// let breadcrumb = '';
		// const pathname = location.pathname;
		const page = new PageInfo(routeData);
		// page.breadcrumb = routes
		// 	.filter(({ path }) => !!path)
		// 	.map(({ path, name, title}, i) => {
		// 		document.title = `${settings.title}` + ( title ? `- ${title}` : '') ;  // set the document title if you want
		// 		if (i < routes.length - 1 && routes[i + 1].path != null) {
		// 			let arr = path.split(/[:/]|(:\/)/g); // sometimes the path is like "/:product_id/details" so I need to extract the interesting part here.
		// 			arr = arr.map((obj) => {
		// 				return (obj in params) ? params[obj] : obj; // We now have ":product_id" and "details" - the first one will exist in the "params" object.
		// 			});
		// 			breadcrumb += arr.filter(Boolean).join('/') + '/';  // clean out some garbage and add the "/" between paths.
		// 			return <Link key={i} to={breadcrumb}>{ i === 0 ? settings.title : name }</Link>;
		// 		} else {
		// 			return <span key={i} className="active">{ i === 0 ? settings.title : name } / </span>;
		// 		}
		// 	},
		// );
		this.setState({ page });
	}

	render(): JSX.Element {
		const { page, isSidebarCollapsed, layout } = this.state;
		const { children, user, navigation, history, location, debugMode, error } = this.props;
		const { key, icon, title, description, headerless } = page;
		const getElemId = (sufix?: string) => `page-${key}${sufix ? '-' + sufix : '' }`;

		if (!layout) {
			return null;
		}

		return (
			<section id="container" className={`${isSidebarCollapsed && layout.menuOrientation !== 'horizontal' ? 'collapsed' : ''} ${layout.menuOrientation}-menu`}>
				{layout.showTopBar &&
				<Header
					title={settings.title}
					icon={settings.icon}
					collapsed={isSidebarCollapsed}
					showLogo={layout.showTopBar}
					user={user}
					routes={layout.menuOrientation === 'horizontal' ? navigation : null}
					activeRoute={location.pathname}
					onLogout={this.handleLogout}
					onTogglePanel={this.handleTogglePanel}
					onRouteChange={history.push}
				/>}
				<main className="main">
					{ this.renderSidebar() }
					<article id={getElemId()} className="page">
						{ title && !headerless && (
							<div id={getElemId('header')} className="page-header">
								{ icon &&
								<span className="page-icon">
									<Icon name={icon} className="page-icon" />
								</span>
								}
								<span id={getElemId('title')} className="page-title">{title}</span>
								<span id={getElemId('subtitle')} className="page-subtitle">{description}</span>
							</div>
						) }
						{ children }
					</article>
					{ layout.showSlidingBar && this.renderSlidingBar() }
				</main>
				{ layout.showFooter && <Footer /> }
				<ErrorTracker debugMode={debugMode} error={error} onClearError={this.handleClearError} />
				<Progress.Component/>
			</section>
		);
	}

	handleClearError() {
		const errorCursor = State.select('error');
		errorCursor.unset();
		State.commit();
	}

	renderSidebar() {
		const { layout, isSidebarCollapsed } = this.state;
		const { /*user,*/ navigation, history, location } = this.props;
		{/* extraInfo={<UserCard user={user}/>} */}

		return (
			layout.menuOrientation === 'vertical' &&
			<Sidebar
				collapsed={isSidebarCollapsed}
				showLogo={!layout.showTopBar}
				routes={navigation}
				title={settings.title}
				icon={settings.icon}
				activeRoute={location.pathname}
				onRouteChange={history.push}
				onTogglePanel={() => this.handleTogglePanel('left')}
				onOpenSettings={this.handleOpenSettings}
			/>
		);
	}

	renderSlidingBar() {
		const { layout } = this.state;

		return (
			<SlidingPanel>
				<Toggler
					id="showTopBar"
					type="flat"
					label="Show top bar"
					checked={ layout.showTopBar }
					onChange={ ({target}) => { this.handleSettingsChange({ showTopBar: target.checked }); } }
				/>
				<Toggler
					id="showFooter"
					type="flat"
					label="Show footer"
					checked={ layout.showFooter }
					onChange={ ({target}) => { this.handleSettingsChange({ showFooter: target.checked }); } }
				/>
				<Toggler
					id="showProfileCard"
					type="flat"
					label="Show profile card"
					checked={ layout.showProfileCard }
					onChange={ ({target}) => { this.handleSettingsChange({ showProfileCard: target.checked }); } }
				/>
				<Toggler
					id="menuOrientation"
					type="flat"
					label="Vertical navigation"
					checked={ layout.menuOrientation === 'vertical' }
					onChange={ ({target}) => { this.handleSettingsChange({ menuOrientation: target.checked ? 'vertical' : 'horizontal' }); } }
				/>
				{/* <ColorSchemaPicker label="Select theme color"/> */}
			</SlidingPanel>
		);
	}

	handleOpenSettings() {
		const { layout } = this.state;
		this.setState({ layout: {...layout, showTopBar: !layout.showTopBar} });
	}

	handleSettingsChange(param: any) {
		const { layout } = this.state;
		this.setState({ layout: {...layout, ...param }}, () => {
			localStorage.setItem(layoutProp, JSON.stringify(this.state.layout));
		});
	}

}

export default root(State, withRouter(WorkspaceLayout));
