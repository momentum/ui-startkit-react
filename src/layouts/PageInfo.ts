export default class PageInfo {
	key: string = '';
	title: string = '';
	description?: string = '';
	icon?: string = '';
	breadcrumb?: string = '';
	headerless: boolean = false;

	constructor(config?: any) {
		Object.assign(this, config);
	}
}
