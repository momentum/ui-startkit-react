import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { autobind } from 'core-decorators';
import { withRouter, RouteProps } from 'react-router';
import { Switch } from 'react-router-dom';
import * as Layout from 'layouts';

import LayoutSettings from './LayoutSettings';

interface Props extends RouteProps {
	routes: any;
	navigation: any;
}

interface State {
	layout: LayoutSettings;
}

// const layoutProp = 'layout';

@autobind
class Viewport extends React.Component<Props, State> {

	state = {
		layout: LayoutSettings.getInstance(),
	};

	constructor(props: Props) {
		super(props);
	}

	async componentDidMount() {
		const node = ReactDOM.findDOMNode(this).parentNode as HTMLElement;

		window.addEventListener('resize', this.handleWindowResize.bind(this, node));
		this.handleWindowResize(node);

		const layoutString = localStorage.getItem('layout');
		if (layoutString) {
			const layout = JSON.parse(layoutString);
			this.setState({layout: Object.assign({}, this.state.layout, layout)});
		}

		// Remove splahscreen from DOM
		const loader = document.getElementById('loader-container') as HTMLElement;
		if (loader) {
			loader.style.opacity = '0';
			setTimeout(() => loader.remove(), 250);
		}
	}

	handleWindowResize(node: HTMLElement) {
		if (!node) {
			return;
		}

		const w = window.innerWidth;
		let viewport;

		switch (true) {
			case (w <= 480) :  viewport = 'tiny'; break;
			case (w > 480 && w <= 667) :  viewport = 'mini'; break;
			case (w > 668 && w <= 768) :  viewport = 'small'; break;
			case (w > 768 && w <= 1024) :  viewport = 'medium'; break;
			case (w > 1024 && w <= 1366) :  viewport = 'default'; break;
			case (w > 1366 && w <= 1824) :  viewport = 'big'; break;
			case (w > 1824) :  viewport = 'large'; break;
		}

		if (!node.dataset.viewport !== viewport) {
			node.dataset.viewport = viewport;
		}
	}

	render(): JSX.Element {
		const { routes, navigation, location } = this.props;
		const protectedRoute = routes.protected.find(({props}) => (props.path === location.pathname) || (props.path !== '/' && location.pathname.indexOf(props.path) > -1));
		const unprotectedRoutes = routes.public.concat(routes.account);

		return (
			<div className="viewport">
				{protectedRoute && (
					<Layout.Workspace navigation={navigation.protected} routeData={{key: protectedRoute.key, ...protectedRoute.props.data}}>
						<Switch>
							{ routes.protected.map((route) => route) }
						</Switch>
					</Layout.Workspace>
				)}
				{!protectedRoute && (
					<Layout.Website>
						<Switch>
							{ unprotectedRoutes.map((route) => route) }
						</Switch>
					</Layout.Website>
				)}
			</div>
		);
	}
}

export default withRouter(Viewport);
