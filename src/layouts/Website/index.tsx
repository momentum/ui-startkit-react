import * as React from 'react';

export default function WebsiteLayout ({ children }) {
	return <div>{ children }</div>;
}
