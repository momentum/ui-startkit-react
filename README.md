# Master UI

## Code References
https://hackernoon.com/import-json-into-typescript-8d465beded79

Git hook to run pre-commit and pre-push commands. By enforcing that tests need to be ran, it prevents bad commits or pushes
https://github.com/typicode/husky

# Base Component

https://www.bountysource.com/issues/26302723-proposal-using-pure-es6-base-classes-over-formsy-mixins

https://medium.com/@franleplant/react-higher-order-components-in-depth-cf9032ee6c3e

https://draftjs.org/docs/overview.html#content


# Vendor and code splitting in webpack 2
https://medium.com/@adamrackis/vendor-and-code-splitting-in-webpack-2-6376358f1923

# Pure CSS toggle buttons
https://codepen.io/mallendeo/pen/eLIiG

# Navigation Menu
https://medialoot.com/blog/how-to-create-a-responsive-navigation-menu-using-only-css/
https://codepen.io/wanni/pen/zsDJb


Principles

Architecture
------
- Context based architecture
- High Order Components (decorators)
- Centralized application settings
- Intentionally, it was not added any state management lib (redux, mobx, baobab, etc) as it can deviate a bit of the main purpose of creating a flexible and accessible
- Async and Await despite of promises or callbacks

Navigation and Layout
-----
- Use of Stateless components whereever possible
- Professional responsive, cross-browser and customizable layout based on Flexbox
	- Menu positioning: vertical | horizontal
	- Footer
	- Topbar
	- Sliding panel (cool for settings and customization features)
- Components suite based on Semantic-UI React
- Flexible routing
- Animated Page transition


Authentication/Authorization
------------
- Flexible routing access control based on user role or claims (suggestion)
- Child component authorizar

Programming Stack
------
- Typescript 2.2.2

- Webpack 2.0 and several cool plugins
	- babel (stage-0),
	- HMR, devServer, hotMiddleware,
	- PostCSS and auto-prefix
	- ...

Best Practices
---------
- Linting: ESLint, TSLint and Stylelining (css and sass)

Testing
---------
- Components unit tests with Jest and Enzyme
- Integration (e2e) testing with nightwatch

https://github.com/archy-bold/ts-jest-test


Data Model

https://gist.github.com/hessryanm/177b0cada8c51516371e

Faster React Components
---------------
https://medium.com/missive-app/45-faster-react-functional-components-now-3509a668e69f

TypeScript Classes & Object-Oriented Programming
--------------
http://www.codebelt.com/typescript/typescript-classes-object-oriented-programming/


# CRUD User Stories
----------------------------------------------------------------------------------------
LP = List Panel
DP = Details Panel
FP = Form Panel

1
WHEN I access the domain page, I EXPECT to get the items loaded in the LP

2
WHEN I click in an item from the LP, I EXPECT to happen:
	1. Selected item row is highlighted in the LP
	2. Selected item details are loaded in the DP
	3. URL is updated and the selected item id is added to it. e.g: /<domain>/:id

3
WHEN I click in the Create button, I EXPECT to happen:
	1. The first item in the LP is selected
	2. A blank form is shown
	3. URL is updated to. e.g: /<domain>/0/new

4
WHEN I refresh the page after selecting an item, I EXPECT to happen:
	1. Selected item row remains highlighted in the LP
	2. Selected item details are loaded in the DP
	3. URL isn't changed

5
WHEN I click in the Edit button, I EXPECT to happen:
	1. Selected item properties are loaded in their correspondent fields the FP
	2. URL is updated and the selected item id and action are added to it. e.g: /<domain>/:id/edit

6
WHEN I change the fields values and hit the Save button, I EXPECT to happen:
	1. If any value is invalid, a warning message is displayed underneath the field and save is aborted
	1. If all fields are valid, selected item details are updated and it remains highlighted in the LP
	2. Selected item details are updated in the FP
	3. A notification toast is displayed to confirm the operation or inform errors

7
WHEN I refresh the page after editing an item, I EXPECT to happen:
	1. Selected item row remains highlighted in the LP
	1. Selected item properties are loaded in their correspondent fields the FP
	3. URL isn't changed

8
WHEN I click in the Cancel button, I EXPECT to happen:
	1. Selected item row remains highlighted in the LP
	2. Selected item details are loaded in the DP
	3. URL is updated and the selected item id is added to it. e.g: /<domain>/:id

9
WHEN I select and item and click in the Delete button, I EXPECT to happen:
	1. A popup window is shown asking for confirmation on the operation.
		1.1 If I DO NOT accept, nothing happens
		1.2 If I DO accept, the item is removed from LP
	2. First item in the LP is selected
	3. URL is updated to reflect the just selected item
10
WHEN I select an item when I'm editing another, I EXPECT
	1. Workflow described in story #2

WHEN I , I EXPECT
WHEN I , I EXPECT
