// https://medium.com/@jameslockwood/getting-react-16-jest-and-enzyme-to-play-nicely-cc6d216ce3c4

import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });